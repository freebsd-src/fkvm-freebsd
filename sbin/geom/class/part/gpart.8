.\" Copyright (c) 2007, 2008 Marcel Moolenaar
.\" All rights reserved.
.\"
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions
.\" are met:
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice, this list of conditions and the following disclaimer.
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in the
.\"    documentation and/or other materials provided with the distribution.
.\"
.\" THIS SOFTWARE IS PROVIDED BY THE AUTHORS AND CONTRIBUTORS ``AS IS'' AND
.\" ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
.\" IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
.\" ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE
.\" FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
.\" DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
.\" OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
.\" HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
.\" LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
.\" OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
.\" SUCH DAMAGE.
.\"
.\" $FreeBSD$
.\"
.Dd Jun 17, 2008
.Dt GPART 8
.Os
.Sh NAME
.Nm gpart
.Nd "control utility for the disk partitioning GEOM class"
.Sh SYNOPSIS
To add support for the disk partitioning GEOM class,
place one or more of the following
lines in your kernel configuration file:
.Bd -ragged -offset indent
.Cd "options GEOM_PART_APM"
.Cd "options GEOM_PART_BSD"
.Cd "options GEOM_PART_GPT"
.Cd "options GEOM_PART_MBR"
.Cd "options GEOM_PART_PC98"
.Cd "options GEOM_PART_VTOC8"
.Ed
.Pp
The GEOM_PART_APM option adds support for the Apple Partition Map (APM)
found on Apple Macintosh computers.
The GEOM_PART_BSD option adds support for the traditional BSD disklabel.
The GEOM_PART_GPT option adds support for the GUID Partition Table (GPT)
found on Intel Itanium computers and Intel-based Macintosh computers.
The GEOM_PART_MBR option adds support for the Master Boot Record (MBR)
found on PCs and used on many removable media.
The GEOM_PART_PC98 option adds support for the MBR variant as used on
NEC PC-98 computers.
The GEOM_PART_VTOC8 option adds support for Sun's SMI VTOC8 label as
found on UltraSPARC-based computers.
.Pp
Usage of the
.Xr gpart 8
utility:
.Pp
.\" ==== ADD ====
.Nm
.Cm add
.Fl b Ar start
.Fl s Ar size
.Fl t Ar type
.Op Fl i Ar index
.Op Fl l Ar label
.Op Fl f Ar flags
.Ar geom
.\" ==== BOOTCODE ====
.Nm
.Cm bootcode
.Op Fl b Ar bootcode
.Op Fl p Ar partcode Fl i Ar index
.Op Fl f Ar flags
.Ar geom
.\" ==== COMMIT ====
.Nm
.Cm commit
.Ar geom
.\" ==== CREATE ====
.Nm
.Cm create
.Fl s Ar scheme
.Op Fl n Ar entries
.Op Fl f Ar flags
.Ar provider
.\" ==== DELETE ====
.Nm
.Cm delete
.Fl i Ar index
.Op Fl f Ar flags
.Ar geom
.\" ==== DESTROY ====
.Nm
.Cm destroy
.Op Fl f Ar flags
.Ar geom
.\" ==== MODIFY ====
.Nm
.Cm modify
.Fl i Ar index
.Op Fl l Ar label
.Op Fl t Ar type
.Op Fl f Ar flags
.Ar geom
.\" ==== SET ====
.Nm
.Cm set
.Fl a Ar attrib
.Fl i Ar index
.Op Fl f Ar flags
.Ar geom
.\" ==== SHOW ====
.Nm
.Cm show
.Op Ar geom ...
.\" ==== UNDO ====
.Nm
.Cm undo
.Ar geom
.\" ==== UNSET ====
.Nm
.Cm unset 
.Fl a Ar attrib
.Fl i Ar index
.Op Fl f Ar flags
.Ar geom
.\"
.Sh DESCRIPTION
The
.Nm
utility is used to partition GEOM providers, normally disks.
The first argument of which is the action to be taken:
.Bl -tag -width ".Cm wwwwwww"
.\" ==== ADD ====
.It Cm add
Add a new partition to the partitioning scheme given by
.Ar geom .
The partition begins on the logical block address given by the
.Fl b Ar start
option.
Its size is expressed in logical block numbers and given by the
.Fl s Ar size
option.
The type of the partition is given by the
.Fl t Ar type
option.
Partition types are discussed in the section entitled "Partition Types".
.Pp
Addition options include:
.Bl -tag -width ".Fl w Ar wwwwwwww"
.It Fl i Ar index
The index in the partition table at which the new partition is to be
placed. The index determines the name of the device special file used
to represent the partition.
.It Fl l Ar label
The label attached to the partition.
This option is only valid when used on partitioning schemes that support
partition labels.
.It Fl f Ar flags
Additional operational flags.
See the section entitled "Operational flags" below for a discussion
about its use.
.El
.\" ==== BOOTCODE ====
.It Cm bootcode
Embed bootstrap code into the partitioning scheme's metadata on the
.Ar geom
(using
.Fl b Ar bootcode )
or write bootstrap code into a partition (using
.Fl p Ar partcode
and
.Fl i Ar index ) .
Not all partitioning schemes have embedded bootstrap code, so the
.Fl b Ar bootcode
option is scheme-specific in nature.
For the GPT scheme, embedded bootstrap code is supported.
The bootstrap code is embedded in the protective MBR rather than the GPT.
The
.Fl b Ar bootcode
option specifies a file that contains the bootstrap code.
The contents and size of the file are determined by the partitioning
scheme.
For the MBR scheme, it's a 512 byte file of which the first 446 bytes
are installed as bootstrap code.
The
.Fl p Ar partcode
option specifies a file that contains the bootstrap code intended to be
written to a partition.
The partition is specified by the
.Fl i Ar index
option.
The size of the file must be smaller than the size of the partition.
.Pp
Addition options include:
.Bl -tag -width ".Fl w Ar wwwwwww"
.It Fl f Ar flags
Additional operational flags.
See the section entitled "Operational flags" below for a discussion
about its use.
.El
.\" ==== COMMIT ====
.It Cm commit
Commit any pending changes for geom
.Ar geom .
All actions are being committed by default and will not result in
pending changes.
Actions can be modified with the
.Fl f Ar flags
option so that they are not being committed by default.
As such, they become pending.
Pending changes are reflected by the geom and the
.Nm
utility, but they are not actually written to disk.
The
.Cm commit
action will write any and all pending changes to disk.
.\" ==== CREATE ====
.It Cm create
Create a new partitioning scheme on a provider given by
.Ar provider .
The
.Fl s Ar scheme
option determines the scheme to use.
The kernel needs to have support for a particular scheme before
that scheme can be used to partition a disk.
.Pp
Addition options include:
.Bl -tag -width ".Fl w Ar wwwwwww"
.It Fl n Ar entries
The number of entries in the partition table.
Every partitioning scheme has a minimum and a maximum number of entries
and this option allows tables to be created with the number of entries
that lies anywhere between the minimum and the maximum.
Some schemes have a maximum equal to the minimum and some schemes have
a maximum large enough to be considered unlimited.
By default, partition tables are created with the minimum number of
entries.
.It Fl f Ar flags
Additional operational flags.
See the section entitled "Operational flags" below for a discussion
about its use.
.El
.\" ==== DELETE ====
.It Cm delete
Delete a partition from geom
.Ar geom
and further identified by the
.Fl i Ar index
option.
The partition cannot be actively used by the kernel.
.Pp
Addition options include:
.Bl -tag -width ".Fl w Ar wwwwwww"
.It Fl f Ar flags
Additional operational flags.
See the section entitled "Operational flags" below for a discussion
about its use.
.El
.\" ==== DESTROY ====
.It Cm destroy
Destroy the partitioning scheme as implemented by geom
.Ar geom .
.Pp
Addition options include:
.Bl -tag -width ".Fl w Ar wwwwwww"
.It Fl f Ar flags
Additional operational flags.
See the section entitled "Operational flags" below for a discussion
about its use.
.El
.\" ==== MODIFY ====
.It Cm modify
Modify a partition from geom
.Ar geom
and further identified by the
.Fl i Ar index
option.
Only the the type and/or label of the partition can be modified.
To change the type of a partition, specify the new type with the
.Fl t Ar type
option.
To change the label of a partition, specify the new label with the
.Fl l Ar label
option.
Not all partitioning schemes support labels and it is invalid to
try to change a partition label in such cases.
.Pp
Addition options include:
.Bl -tag -width ".Fl w Ar wwwwwww"
.It Fl f Ar flags
Additional operational flags.
See the section entitled "Operational flags" below for a discussion
about its use.
.El
.\" ==== SET ====
.It Cm set
Set the named attribute on the partition entry.
.Pp
Addition options include:
.Bl -tag -width ".Fl w Ar wwwwwww"
.It Fl f Ar flags
Additional operational flags.
See the section entitled "Operational flags" below for a discussion
about its use.
.El
.\" ==== SHOW ====
.It Cm show
Show the current partition information of the specified geoms
or all geoms if none are specified.
.\" ==== UNDO ====
.It Cm undo
Revert any pending changes.
This action is the opposite of the
.Cm commit
action and can be used to undo any changes that have not been committed.
.\" ==== UNSET ====
.It Cm unset
Clear the named attribute on the partition entry.
.Pp
Addition options include:
.Bl -tag -width ".Fl w Ar wwwwwww"
.It Fl f Ar flags
Additional operational flags.
See the section entitled "Operational flags" below for a discussion
about its use.
.El
.El
.\"
.Sh PARTITION TYPES
The
.Nm
utility uses symbolic names for common partition types to avoid that the
user needs to know what the partitioning scheme in question is and what
the actual number or identification needs to be used for a particular
type.
the
.Nm
utility also allows the user to specify scheme-specific partition types
for partition types that don't have symbol names.
The symbolic names currently understood are:
.Bl -tag -width "wwwwwwwwwwwww"
.It efi
The system partition for computers that use the Extensible Firmware
Interface (EFI).
In such cases, the GPT partitioning scheme is being used and the
actual partition type for the system partition can also be specified as
"!c12a7328-f81f-11d2-ba4b-00a0c93ec93ab".
.It freebsd
A FreeBSD partition that uses the BSD disklabel to sub-divide the
partition into file systems.
This is a legacy partition type and should not be used for the APM
or GPT schemes.
The scheme-specific types are "!165" for MBR, "!FreeBSD" for APM, and
"!516e7cb4-6ecf-11d6-8ff8-00022d09712b" for GPT.
.It freebsd-boot
A FreeBSD partition dedicated to bootstrap code.
The scheme-specific type is "!83bd6b9d-7f41-11dc-be0b-001560b84f0f" for GPT.
.It freebsd-swap
A FreeBSD partition dedicated to swap space.
The scheme-specific types are "!FreeBSD-swap" for APM, and
"!516e7cb5-6ecf-11d6-8ff8-00022d09712b" for GPT.
.It freebsd-ufs
A FreeBSD partition that contains a UFS or UFS2 file system.
the scheme-specific types are "!FreeBSD-UFS" for APM, and
"!516e7cb6-6ecf-11d6-8ff8-00022d09712b" for GPT.
.It freebsd-vinum
A FreeBSD partition that contains a Vinum volume.
The scheme-specific types are "!FreeBSD-Vinum" for APM, and
"!516e7cb8-6ecf-11d6-8ff8-00022d09712b" for GPT.
.It freebsd-zfs
A FreeBSD partition that contains a ZFS volume.
The scheme-specific types are "!FreeBSD-ZFS" for APM, and
"!516e7cba-6ecf-11d6-8ff8-00022d09712b" for GPT.
.It mbr
A partition that is sub-partitioned by a master boot record (MBR).
This type is known as "!024dee41-33e7-11d3-9d69-0008c781f39f" by GPT.
.El
.Sh OPERATIONAL FLAGS
Actions other than the
.Cm commit
and
.Cm undo
actions take an optional
.Fl f Ar flags
option.
This option is used to specify action-specific operational flags.
By default, the
.Nm
utility defines the 'C' flag so that the action is immediately
committed.
The user can specify
.Fl f Ar x
to have the action result in a pending change that can later, with
other pending changes, be committed as a single compound change with
the
.Cm commit
action or reverted with the
.Cm undo
action.
.Sh EXIT STATUS
Exit status is 0 on success, and 1 if the command fails.
.Sh SEE ALSO
.Xr geom 4 ,
.Xr geom 8 ,
.Sh HISTORY
The
.Nm
utility appeared in
.Fx 7.0 .
.Sh AUTHORS
.An Marcel Moolenaar Aq marcel@FreeBSD.org

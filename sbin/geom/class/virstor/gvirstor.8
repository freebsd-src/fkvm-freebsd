.\" Copyright (c) 2005 Pawel Jakub Dawidek <pjd@FreeBSD.org>
.\" Copyright (c) 2005 Ivan Voras <ivoras@FreeBSD.org>
.\" All rights reserved.
.\"
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions
.\" are met:
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice, this list of conditions and the following disclaimer.
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in the
.\"    documentation and/or other materials provided with the distribution.
.\"
.\" THIS SOFTWARE IS PROVIDED BY THE AUTHORS AND CONTRIBUTORS ``AS IS'' AND
.\" ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
.\" IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
.\" ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE
.\" FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
.\" DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
.\" OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
.\" HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
.\" LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
.\" OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
.\" SUCH DAMAGE.
.\"
.\" $FreeBSD$
.\"
.Dd July 8, 2006
.Dt GVIRSTOR 8
.Os
.Sh NAME
.Nm gvirstor
.Nd "provides virtual data storage geom"
.Sh SYNOPSIS
.Nm
.Cm label
.Op Fl hv
.Op Fl s Ar virsize
.Op Fl m Ar chunksize
.Ar name
.Ar prov ...
.Nm
.Cm stop
.Op Fl fv
.Ar name ...
.Nm
.Cm add
.Op Fl vh
.Ar name prov ...
.Nm
.Cm remove
.Op Fl v
.Ar name prov ...
.Nm
.Cm clear
.Op Fl v
.Ar prov ...
.Nm
.Cm dump
.Ar prov ...
.Nm
.Cm list
.Nm
.Cm status
.Nm
.Cm load
.Nm
.Cm unload
.Sh DESCRIPTION
The
.Nm
utility is used for setting up a storage device of arbitrary large size (for example,
several TB), consisting of an arbitrary number of physical storage devices with
total size <= the virtual size. Data for the virtual devices will be allocated from
physical devices on demand. In short, this is the virtual storage functionality.
The first argument to
.Nm
indicates an action to be performed:
.Bl -tag -width ".Cm destroy"
.It Cm label
Set up a virtual device from the given components with the specified
.Ar name .
Metadata are stored in the last sector of every component.
Argument
.Ar virsize
is the size of new virtual device, with default being 2 TiB (2097152 MiB).
Argument
.Ar chunksize
is the chunk size, with default being 4 MiB (4096 KiB).
The default is thus "-s 2097152 -m 4096".
.It Cm stop
Turn off an existing virtual device by its
.Ar name .
This command does not touch on-disk metadata.
As with other GEOM classes, stopped geoms cannot be started manually.
.It Cm add
Adds new components to existing virtual device by its
.Ar name .
The specified virstor device must exist and be active (i.e.
module loaded, device present in /dev).
.It Cm remove
Removes components from existing virtual device by its
.Ar name .
Only unallocated providers can be removed.
.It Cm clear
Clear metadata on the given providers.
.It Cm dump
Dump metadata stored on the given providers.
.It Cm list
See
.Xr geom 8 .
.It Cm status
See
.Xr geom 8 .
.It Cm load
See
.Xr geom 8 .
.It Cm unload
See
.Xr geom 8 .
.El
.Pp
Additional options:
.Bl -tag -width ".Fl f"
.It Fl f
Force the removal of the specified virtual device.
.It Fl h
Hardcode providers' names in metadata.
.It Fl v
Be more verbose.
.El
.Sh EXIT STATUS
Exit status is 0 on success, and 1 if the command fails.
.Sh EXAMPLES
The following example shows how to create a virtual device of default size
(2 TiB), of default chunk (extent) size (4 MiB), with two physical devices for
backing storage.
.Bd -literal -offset indent
gvirstor label -v mydata /dev/ad4 /dev/ad6
newfs /dev/virstor/mydata
.Ed
.Pp
From now on, the virtual device will be available via the
.Pa /dev/virstor/mydata
device entry.
To add a new physical device / provider to an active virstor device:
.Bd -literal -offset indent
gvirstor add mydata ad8
.Ed
.Pp
This will add physical storage (from ad8) to 
.Pa /dev/virstor/mydata
device.
To see device status information (including how much physical storage 
is still available for the virtual device), use:
.Bd -literal -offset indent
gvirstor list
.Ed
.Pp
All standard
.Xr geom 8
subcommands (e.g. "status", "help") are also supported.
.Sh SYSCTLs
.Nm
has several 
.Xr sysctl 8
tunable variables.
.Bd -literal -offset indent
.Pa int kern.geom.virstor.debug
.Ed
.Pp
This sysctl controls verbosity of the kernel module, in the range
1 to 15. Messages that are marked with higher verbosity levels than 
this are supressed. Default value is 5 and it's not
recommented to set this tunable to less than 2, because level 1 messages 
are error events, and level 2 messages are system warnings.
.Bd -literal -offset indent
.Pa int kern.geom.virstor.chunk_watermark
.Ed
.Pp
Value in this sysctl sets warning watermark level for physical chunk usage
on a single component. The warning is issued when a virstor component
has less than this many free chunks (default 100).
.Bd -literal -offset indent
.Pa int kern.geom.virstor.component_watermark
.Ed
.Pp
Value in this sysctl sets warning watermark level for component usage.
The warning is issed when there are less than this many unallocated
components (default is 1).
.Pp
All these sysctls are also available as
.Xr loader 8
tunables.
.Sh LOG MESSAGES
.Nm
kernel module issues log messages with prefixes in standardised format,
which is useful for log message filtering and dispatching. Each message
line begins with
.Bd -literal -offset indent
.Pa GEOM_VIRSTOR[%d]: 
.Ed
.Pp
The number (%d) is message verbosity / importance level, in the range
1 to 15. If a message filtering, dispatching or operator alert system is
used, it is recommended that messages with levels 1 and 2 be taken
seriously (for example, to catch out-of-space conditions as set by
watermark sysctls).
.Sh SEE ALSO
.Xr geom 4 ,
.Xr geom 8 ,
.Xr newfs 8 ,
.Xr fstab 5 ,
.Xr glabel 8
.Sh HISTORY
The
.Nm
utility appeared in
.Fx 7.0 .
.Sh BUGS
Commands "add" and "remove" contain unavoidable critical sections
which may make the virstor device unusable if a power failure (or
other disruptive event) happens during their execution. 
It's recommended to run them when the system is quiescent.
.Sh AUTHOR
.An Ivan Voras Aq ivoras@FreeBSD.org 
Sponsored by Google Summer of Code 2006

/*-
 * Copyright (c) 2008 Brent Stephens <brents@rice.edu>
 * Copyright (c) 2008 Diego Ongaro <diego.ongaro@rice.edu>
 * Copyright (c) 2008 Kaushik Kumar Ram <kaushik@rice.edu>
 * Copyright (c) 2008 Oleg Pesok <olegpesok@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef VMCB_H
#define VMCB_H

struct __attribute__ ((__packed__)) vmcb_control_area {
	uint16_t	intercept_cr_reads;
	uint16_t	intercept_cr_writes;
	uint16_t	intercept_dr_reads;
	uint16_t	intercept_dr_writes;
	uint32_t	intercept_exceptions;
	uint64_t	intercepts;
	uint8_t		reserved_1[44]; /* Should be Zero */
	uint64_t	iopm_base_pa;
	uint64_t	msrpm_base_pa;
	uint64_t	tsc_offset;
	uint32_t	guest_asid;
	uint8_t		tlb_control;
	uint8_t		reserved_2[3]; /* Should be Zero */
	uint8_t		v_tpr;
	uint8_t		v_irq_pending;
	uint8_t		v_intr;
	uint8_t		v_intr_masking;
	uint8_t		v_intr_vector;
	uint8_t		reserved_6[3]; /* Should be Zero */
	uint8_t		intr_shadow; /* Bits 7-1 Should be zero */
	uint8_t		reserved_7[7];
	uint64_t	exit_code; /* TODO: in kvm this is 2 uint32. figure out why */
	uint64_t	exit_info_1;
	uint64_t	exit_info_2;
	uint32_t	exit_int_info;
	uint32_t	exit_int_info_err_code;
	uint64_t	nested_ctl;
	uint8_t		reserved_8[16];
	uint64_t	event_inj; /* TODO: in kvm this is 2 uint32 */
	uint64_t	nested_cr3;
	uint64_t	lbr_virt_enable;
	uint8_t		reserved_9[832];
};

struct __attribute__ ((__packed__)) vmcb_seg {
	uint16_t	selector;
	uint16_t	attrib;
	uint32_t	limit;
	uint64_t	base;
};

struct __attribute__ ((__packed__)) vmcb_save_area {
	struct vmcb_seg		es;
	struct vmcb_seg		cs;
	struct vmcb_seg		ss;
	struct vmcb_seg		ds;
	struct vmcb_seg		fs;
	struct vmcb_seg		gs;
	struct vmcb_seg		gdtr;
	struct vmcb_seg		ldtr;
	struct vmcb_seg		idtr;
	struct vmcb_seg		tr;
	uint8_t			reserved_1[43];
	uint8_t			cpl;
	uint8_t			reserved_2[4];
	uint64_t		efer;
	uint8_t			reserved_3[112];
	uint64_t		cr4;
	uint64_t		cr3;
	uint64_t		cr0;
	uint64_t		dr7;
	uint64_t		dr6;
	uint64_t		rflags;
	uint64_t		rip;
	uint8_t			reserved_4[88];
	uint64_t		rsp;
	uint8_t			reserved_5[24];
	uint64_t		rax;
	uint64_t		star;
	uint64_t		lstar;
	uint64_t		cstar;
	uint64_t		sfmask;
	uint64_t		kernel_gs_base;
	uint64_t		sysenter_cs;
	uint64_t		sysenter_esp;
	uint64_t		sysenter_eip;
	uint64_t		cr2;
	uint8_t			reserved_6[32];
	uint64_t		g_pat;
	uint64_t		dbg_ctl;
	uint64_t		br_from;
	uint64_t		br_to;
	uint64_t		last_excp_from;
	uint64_t		last_excp_to;
};

struct __attribute__ ((__packed__)) vmcb {
	struct vmcb_control_area 	control;
	struct vmcb_save_area 		save;
};

/***********************************/
/************ Intercepts ***********/
/***********************************/
#define MAKE_INTERCEPT(x)		(1ULL << (x))
#define INTERCEPT_NOTHING		(0)
#define	INTERCEPT_INTR			MAKE_INTERCEPT(0)
#define	INTERCEPT_NMI			MAKE_INTERCEPT(1)
#define	INTERCEPT_SMI			MAKE_INTERCEPT(2)
#define	INTERCEPT_INIT			MAKE_INTERCEPT(3)
#define	INTERCEPT_VINTR			MAKE_INTERCEPT(4)
#define	INTERCEPT_CR0			MAKE_INTERCEPT(5)
#define	INTERCEPT_READ_IDTR		MAKE_INTERCEPT(6)
#define	INTERCEPT_READ_GDTR		MAKE_INTERCEPT(7)
#define	INTERCEPT_READ_LDTR		MAKE_INTERCEPT(8)
#define	INTERCEPT_READ_TR		MAKE_INTERCEPT(9)
#define	INTERCEPT_WRITE_IDTR		MAKE_INTERCEPT(10)
#define	INTERCEPT_WRITE_GDTR		MAKE_INTERCEPT(11)
#define	INTERCEPT_WRITE_LDTR		MAKE_INTERCEPT(12)
#define	INTERCEPT_WRITE_TR		MAKE_INTERCEPT(13)
#define	INTERCEPT_RDTSC			MAKE_INTERCEPT(14)
#define	INTERCEPT_RDPMC			MAKE_INTERCEPT(15)
#define	INTERCEPT_PUSHF			MAKE_INTERCEPT(16)
#define	INTERCEPT_POPF			MAKE_INTERCEPT(17)
#define	INTERCEPT_CPUID			MAKE_INTERCEPT(18)
#define	INTERCEPT_RSM			MAKE_INTERCEPT(19)
#define	INTERCEPT_IRET			MAKE_INTERCEPT(20)
#define	INTERCEPT_INTn			MAKE_INTERCEPT(21)
#define	INTERCEPT_INVD			MAKE_INTERCEPT(22)
#define	INTERCEPT_PAUSE			MAKE_INTERCEPT(23)
#define	INTERCEPT_HLT			MAKE_INTERCEPT(24)
#define	INTERCEPT_INVLPG		MAKE_INTERCEPT(25)
#define	INTERCEPT_INVLPGA		MAKE_INTERCEPT(26)
#define	INTERCEPT_IOIO_PROT		MAKE_INTERCEPT(27)
#define	INTERCEPT_MSR_PROT		MAKE_INTERCEPT(28)
#define	INTERCEPT_TASK_SWITCH		MAKE_INTERCEPT(29)
#define	INTERCEPT_FERR_FREEZE		MAKE_INTERCEPT(30)
#define	INTERCEPT_SHUTDOWN		MAKE_INTERCEPT(31)
#define	INTERCEPT_VMRUN			MAKE_INTERCEPT(32)
#define	INTERCEPT_VMMCALL		MAKE_INTERCEPT(33)
#define	INTERCEPT_VMLOAD		MAKE_INTERCEPT(34)
#define	INTERCEPT_VMSAVE		MAKE_INTERCEPT(35)
#define	INTERCEPT_STGI			MAKE_INTERCEPT(36)
#define	INTERCEPT_CLGI			MAKE_INTERCEPT(37)
#define	INTERCEPT_SKINIT		MAKE_INTERCEPT(38)
#define	INTERCEPT_RDTSCP		MAKE_INTERCEPT(39)
#define	INTERCEPT_ICEBP			MAKE_INTERCEPT(40)
#define	INTERCEPT_WBINVD		MAKE_INTERCEPT(41)
#define	INTERCEPT_MONITOR		MAKE_INTERCEPT(42)
#define	INTERCEPT_MWAIT_UNCOND		MAKE_INTERCEPT(43)
#define	INTERCEPT_MWAIT_COND		MAKE_INTERCEPT(44)

#define INTERCEPT_CR0_MASK		MAKE_INTERCEPT(0)
#define INTERCEPT_CR3_MASK		MAKE_INTERCEPT(3)
#define INTERCEPT_CR4_MASK		MAKE_INTERCEPT(4)
#define INTERCEPT_CR8_MASK		MAKE_INTERCEPT(8)

#define INTERCEPT_DR0_MASK		MAKE_INTERCEPT(0)
#define INTERCEPT_DR1_MASK		MAKE_INTERCEPT(1)
#define INTERCEPT_DR2_MASK		MAKE_INTERCEPT(2)
#define INTERCEPT_DR3_MASK		MAKE_INTERCEPT(3)
#define INTERCEPT_DR4_MASK		MAKE_INTERCEPT(4)
#define INTERCEPT_DR5_MASK		MAKE_INTERCEPT(5)
#define INTERCEPT_DR6_MASK		MAKE_INTERCEPT(6)
#define INTERCEPT_DR7_MASK		MAKE_INTERCEPT(7)

/***********************************/
/************ Exit Codes ***********/
/***********************************/
#define	VMCB_EXIT_READ_CR0		0x000
#define	VMCB_EXIT_READ_CR3		0x003
#define	VMCB_EXIT_READ_CR4		0x004
#define	VMCB_EXIT_READ_CR8		0x008
#define	VMCB_EXIT_WRITE_CR0		0x010
#define	VMCB_EXIT_WRITE_CR3		0x013
#define	VMCB_EXIT_WRITE_CR4		0x014
#define	VMCB_EXIT_WRITE_CR8		0x018
#define	VMCB_EXIT_READ_DR0		0x020
#define	VMCB_EXIT_READ_DR1		0x021
#define	VMCB_EXIT_READ_DR2		0x022
#define	VMCB_EXIT_READ_DR3		0x023
#define	VMCB_EXIT_READ_DR4		0x024
#define	VMCB_EXIT_READ_DR5		0x025
#define	VMCB_EXIT_READ_DR6		0x026
#define	VMCB_EXIT_READ_DR7		0x027
#define	VMCB_EXIT_WRITE_DR0		0x030
#define	VMCB_EXIT_WRITE_DR1		0x031
#define	VMCB_EXIT_WRITE_DR2		0x032
#define	VMCB_EXIT_WRITE_DR3		0x033
#define	VMCB_EXIT_WRITE_DR4		0x034
#define	VMCB_EXIT_WRITE_DR5		0x035
#define	VMCB_EXIT_WRITE_DR6		0x036
#define	VMCB_EXIT_WRITE_DR7		0x037
#define VMCB_EXIT_EXCP_BASE		0x040
#define VMCB_EXIT_INTR			0x060
#define VMCB_EXIT_NMI			0x061
#define VMCB_EXIT_SMI			0x062
#define VMCB_EXIT_INIT			0x063
#define VMCB_EXIT_VINTR			0x064
#define VMCB_EXIT_CR0_SEL_WRITE		0x065
#define VMCB_EXIT_IDTR_READ		0x066
#define VMCB_EXIT_GDTR_READ		0x067
#define VMCB_EXIT_LDTR_READ		0x068
#define VMCB_EXIT_TR_READ		0x069
#define VMCB_EXIT_IDTR_WRITE		0x06a
#define VMCB_EXIT_GDTR_WRITE		0x06b
#define VMCB_EXIT_LDTR_WRITE		0x06c
#define VMCB_EXIT_TR_WRITE		0x06d
#define VMCB_EXIT_RDTSC			0x06e
#define VMCB_EXIT_RDPMC			0x06f
#define VMCB_EXIT_PUSHF			0x070
#define VMCB_EXIT_POPF			0x071
#define VMCB_EXIT_CPUID			0x072
#define VMCB_EXIT_RSM			0x073
#define VMCB_EXIT_IRET			0x074
#define VMCB_EXIT_SWINT			0x075
#define VMCB_EXIT_INVD			0x076
#define VMCB_EXIT_PAUSE			0x077
#define VMCB_EXIT_HLT			0x078
#define VMCB_EXIT_INVLPG		0x079
#define VMCB_EXIT_INVLPGA		0x07a
#define VMCB_EXIT_IOIO			0x07b
#define VMCB_EXIT_MSR			0x07c
#define VMCB_EXIT_TASK_SWITCH		0x07d
#define VMCB_EXIT_FERR_FREEZE		0x07e
#define VMCB_EXIT_SHUTDOWN		0x07f
#define VMCB_EXIT_VMRUN			0x080
#define VMCB_EXIT_VMMCALL		0x081
#define VMCB_EXIT_VMLOAD		0x082
#define VMCB_EXIT_VMSAVE		0x083
#define VMCB_EXIT_STGI			0x084
#define VMCB_EXIT_CLGI			0x085
#define VMCB_EXIT_SKINIT		0x086
#define VMCB_EXIT_RDTSCP		0x087
#define VMCB_EXIT_ICEBP			0x088
#define VMCB_EXIT_WBINVD		0x089
#define VMCB_EXIT_MONITOR		0x08a
#define VMCB_EXIT_MWAIT_UNCOND		0x08b
#define VMCB_EXIT_MWAIT_COND		0x08c
#define VMCB_EXIT_NPF			0x400
#define VMCB_EXIT_ERR			-1


#define VMCB_MAKE_EXITINTINFO_TYPE(x)		((x) << 8)
#define VMCB_EXITINTINFO_VEC_MASK		0xff
#define	VMCB_EXITINTINFO_TYPE_INTR		VMCB_MAKE_EXITINTINFO_TYPE(0)
#define	VMCB_EXITINTINFO_TYPE_NMI		VMCB_MAKE_EXITINTINFO_TYPE(2)
#define	VMCB_EXITINTINFO_TYPE_EXEPT		VMCB_MAKE_EXITINTINFO_TYPE(3)
#define	VMCB_EXITINTINFO_TYPE_SOFT		VMCB_MAKE_EXITINTINFO_TYPE(4)
#define VMCB_EXITINTINFO_VALID			(1 << 31)
#define VMCB_EXITINTINFO_ERR_VALID		(1 << 11)

#define VMCB_EVTINJ_VEC_MASK			VMCB_EXITINTINFO_VEC_MASK
#define VMCB_EVTINJ_TYPE_INTR			VMCB_EXITINTINFO_TYPE_INTR
#define VMCB_EVTINJ_TYPE_NMI			VMCB_EXITINTINFO_TYPE_NMI
#define VMCB_EVTINJ_TYPE_EXEPT			VMCB_EXITINTINFO_TYPE_EXEPT
#define VMCB_EVTINJ_TYPE_SOFT			VMCB_EXITINTINFO_TYPE_SOFT
#define VMCB_EVTINJ_VALID			VMCB_EXITINTINFO_VALID
#define VMCB_EVTINJ_VALID_ERR			VMCB_EXITINTINFO_ERR_VALID

/* TODO: finish making these error codes */
#define VMCB_EXITINFO_TS_MASK_IRET		(1 << 36)
#define VMCB_EXITINFO_TS_MASK_JMP		(1 << 38)

#define	VMCB_TLB_CONTROL_DO_NOTHING		0
#define VMCB_TLB_CONTROL_FLUSH_ALL		1

#define VMCB_SELECTOR_S_SHIFT 4
#define VMCB_SELECTOR_DPL_SHIFT 5
#define VMCB_SELECTOR_P_SHIFT 7
#define VMCB_SELECTOR_AVL_SHIFT 8
#define VMCB_SELECTOR_L_SHIFT 9
#define VMCB_SELECTOR_DB_SHIFT 10
#define VMCB_SELECTOR_G_SHIFT 11
#define VMCB_SELECTOR_TYPE_MASK (0xf)
#define VMCB_SELECTOR_S_MASK (1 << VMCB_SELECTOR_S_SHIFT)
#define VMCB_SELECTOR_DPL_MASK (3 << VMCB_SELECTOR_DPL_SHIFT)
#define VMCB_SELECTOR_P_MASK (1 << VMCB_SELECTOR_P_SHIFT)
#define VMCB_SELECTOR_AVL_MASK (1 << VMCB_SELECTOR_AVL_SHIFT)
#define VMCB_SELECTOR_L_MASK (1 << VMCB_SELECTOR_L_SHIFT)
#define VMCB_SELECTOR_DB_MASK (1 << VMCB_SELECTOR_DB_SHIFT)
#define VMCB_SELECTOR_G_MASK (1 << VMCB_SELECTOR_G_SHIFT)
#define VMCB_SELECTOR_WRITE_MASK (1 << 1)
#define VMCB_SELECTOR_READ_MASK VMCB_SELECTOR_WRITE_MASK
#define VMCB_SELECTOR_CODE_MASK (1 << 3)

#endif              /* VMCB_H */

/*-
 * Copyright (c) 2008 Brent Stephens <brents@rice.edu>
 * Copyright (c) 2008 Diego Ongaro <diego.ongaro@rice.edu>
 * Copyright (c) 2008 Kaushik Kumar Ram <kaushik@rice.edu>
 * Copyright (c) 2008 Oleg Pesok <olegpesok@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef _FKVM_H_
#define _FKVM_H_

#include <sys/types.h>

#define KVM_NR_INTERRUPTS 256
#define MAX_VCPUS 8

/* MSR */

struct kvm_msr_list {
	uint32_t nmsrs; /* number of msrs in entries */
	uint32_t indices[0];
};

struct kvm_msr_entry {
	uint32_t index;
	uint32_t reserved;
	uint64_t data;
};

struct kvm_msrs {
	uint32_t nmsrs; /* number of msrs in entries */
	uint32_t pad;

	struct kvm_msr_entry entries[0];
};

#if defined(_KERNEL) || defined(FKVM_INTERNAL)
enum {
	FKVM_REGS_TYPE_REGS=1,
	FKVM_REGS_TYPE_SREGS=2,
	FKVM_REGS_TYPE_MSRS=3,
};
#endif

/* get/set regs */

enum {
	KVM_REG_RAX = 0,
	KVM_REG_RBX = 1,
	KVM_REG_RCX = 2,
	KVM_REG_RDX = 3,
	KVM_REG_RSI = 4,
	KVM_REG_RDI = 5,
	KVM_REG_RSP = 6,
	KVM_REG_RBP = 7,
	KVM_REG_R8  = 8,
	KVM_REG_R9  = 9,
	KVM_REG_R10 = 10,
	KVM_REG_R11 = 11,
	KVM_REG_R12 = 12,
	KVM_REG_R13 = 13,
	KVM_REG_R14 = 14,
	KVM_REG_R15 = 15,
	KVM_REG_RIP = 16,
	KVM_REG_RFLAGS = 17
};

struct kvm_regs {
	uint64_t rax;
	uint64_t rbx;
	uint64_t rcx;
	uint64_t rdx;
	uint64_t rsi;
	uint64_t rdi;
	uint64_t rsp;
	uint64_t rbp;
	uint64_t r8;
	uint64_t r9;
	uint64_t r10;
	uint64_t r11;
	uint64_t r12;
	uint64_t r13;
	uint64_t r14;
	uint64_t r15;
	uint64_t rip;
	uint64_t rflags;
};

static inline void
kvm_regs_set(struct kvm_regs *regs, int idx, uint64_t value)
{
	*(((uint64_t*) regs) + idx) = value;
}

static inline uint64_t
kvm_regs_get(struct kvm_regs *regs, int idx)
{
	return *(((uint64_t*) regs) + idx);
}

/* get/set fpu */

struct kvm_fpu {
	uint8_t  fpr[8][16];
	uint16_t fcw;
	uint16_t fsw;
	uint8_t  ftwx;  /* in fxsave format */
	uint8_t  pad1;
	uint16_t last_opcode;
	uint64_t last_ip;
	uint64_t last_dp;
	uint8_t  xmm[16][16];
	uint32_t mxcsr;
	uint32_t pad2;
};

/* get/set sregs */

struct kvm_segment {
	uint64_t base;
	uint32_t limit;
	uint16_t selector;
	uint8_t  type;
	uint8_t  present;
	uint8_t  dpl;
	uint8_t  db;
	uint8_t  s;
	uint8_t  l;
	uint8_t  g;
	uint8_t  avl;
	uint8_t  unusable;
	uint8_t  padding;
};

struct kvm_dtable {
	uint64_t base;
	uint16_t limit;
	uint16_t padding[3];
};

struct kvm_sregs {
	struct kvm_segment cs;
	struct kvm_segment ds;
	struct kvm_segment es;
	struct kvm_segment fs;
	struct kvm_segment gs;
	struct kvm_segment ss;
	struct kvm_segment tr;
	struct kvm_segment ldt;
	struct kvm_dtable  gdt;
	struct kvm_dtable  idt;
	uint64_t cr0;
	uint64_t cr2;
	uint64_t cr3;
	uint64_t cr4;
	uint64_t cr8;
	uint64_t dr0;
	uint64_t dr1;
	uint64_t dr2;
	uint64_t dr3;
	uint64_t dr6;
	uint64_t dr7;
	uint64_t efer;
	uint64_t apic_base;
	uint64_t interrupt_bitmap[(KVM_NR_INTERRUPTS + 63) / 64];
};

/* debug */

struct kvm_breakpoint {
	uint32_t enabled;
	uint32_t padding;
	uint64_t address;
};

struct kvm_debug_guest {
	uint32_t enabled;
	uint32_t pad;
	struct kvm_breakpoint breakpoints[4];
	uint32_t singlestep;
};

/* cpuid */

struct kvm_cpuid_entry {
	uint32_t function;
	uint32_t eax;
	uint32_t ebx;
	uint32_t ecx;
	uint32_t edx;
	uint32_t padding;
};

#if defined(_KERNEL) || defined(FKVM_INTERNAL)

enum {
	KVM_EXIT_UNKNOWN         = 0,
	KVM_EXIT_EXCEPTION       = 1,
	KVM_EXIT_IO              = 2,
	KVM_EXIT_HYPERCALL       = 3,
	KVM_EXIT_DEBUG           = 4,
	KVM_EXIT_HLT             = 5,
	KVM_EXIT_MMIO            = 6,
	KVM_EXIT_IRQ_WINDOW_OPEN = 7,
	KVM_EXIT_SHUTDOWN        = 8,
	KVM_EXIT_FAIL_ENTRY      = 9,
	KVM_EXIT_INTR            = 10,
	KVM_EXIT_S390_SIEIC      = 11,
	KVM_EXIT_S390_RESET      = 12,
	KVM_EXIT_DCR             = 13,
	KVM_EXIT_NMI             = 14,
	KVM_EXIT_NMI_WINDOW_OPEN = 15,
	KVM_EXIT_CPUID           = 16,
	KVM_EXIT_CR              = 17,
	KVM_EXIT_DR              = 18,
	KVM_EXIT_EXCP            = 19,
	KVM_EXIT_CONTINUE        = 20,
};

struct kvm_run {
	/* in */
	uint8_t request_interrupt_window;
	uint8_t request_nmi_window;
	uint8_t padding1[6];

	/* out */
	uint32_t exit_reason;
	uint8_t ready_for_interrupt_injection;
	uint8_t if_flag;
	uint8_t ready_for_nmi_injection;
	uint8_t padding2;

	/* in (pre_kvm_run), out (post_kvm_run) */
	uint64_t cr8;

	union {
		/* KVM_EXIT_UNKNOWN */
		struct {
			uint64_t hardware_exit_reason;
		} hw;
		/* KVM_EXIT_FAIL_ENTRY */
		struct {
			uint64_t hardware_entry_failure_reason;
		} fail_entry;
		/* KVM_EXIT_EXCEPTION */
		struct {
			uint32_t exception;
			uint32_t error_code;
		} ex;
		/* KVM_EXIT_IO */
		struct kvm_io {
#define KVM_EXIT_IO_IN  0
#define KVM_EXIT_IO_OUT 1
			uint8_t  in; /* 1 = in 0 = out */
			uint8_t  size; /* bytes */
			uint8_t  string; /* 1 = string */
			uint8_t  rep; /* repeat boolean */
			uint16_t port;
			uint64_t next_rip;

		} io;
		/* KVM_EXIT_MMIO */
		struct {
			uint64_t fault_gpa; /* Faulting Guest Physical Address */
			uint64_t rip;
			uint64_t cs_base;
		} mmio;
		/* KVM_EXIT_DCR */
		struct {
			uint32_t dcrn;
			uint32_t data;
			uint8_t  is_write;
		} dcr;
		/* KVM_EXIT_CPUID */
		struct {
			uint32_t fn;
		} cpuid;
		/* KVM_EXIT_CR */
		struct {
			uint8_t cr_num;
			uint8_t is_write;
		} cr;
		/* KVM_EXIT_DR */
		struct {
			uint8_t dr_num;
			uint8_t is_write;
		} dr;
		/* KVM_EXIT_EXCP */
		struct {
			uint8_t vector;
		} excp;
		/* Fix the size of the union. */
		uint8_t padding[32];
	} u;
};
#endif

#endif /* !_FKVM_H_ */

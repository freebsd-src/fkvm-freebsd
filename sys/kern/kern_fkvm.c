/*-
 * Copyright (c) 2008 Brent Stephens <brents@rice.edu>
 * Copyright (c) 2008 Diego Ongaro <diego.ongaro@rice.edu>
 * Copyright (c) 2008 Kaushik Kumar Ram <kaushik@rice.edu>
 * Copyright (c) 2008 Oleg Pesok <olegpesok@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/fkvm.h>
#include <sys/cdefs.h>
#include <sys/param.h>
#include <sys/systm.h>
#include <sys/kernel.h>
#include <sys/malloc.h>
#include <sys/sysproto.h>
#include <sys/file.h>
#include <sys/mman.h>
#include <sys/proc.h>
#include <sys/eventhandler.h>
#include <vm/vm.h>
#include <vm/pmap.h>
#include <vm/vm_extern.h>
#include <vm/vm_map.h>
#include <vm/vm_object.h>
#include <vm/vm_param.h>
#include <machine/_inttypes.h>
#include <machine/specialreg.h>
#include <machine/segments.h>
#include <machine/vmcb.h>


/* Definitions for Port IO */
#define PORT_SHIFT              16
#define ADDR_SHIFT		7
#define SIZE_SHIFT              4
#define REP_SHIFT               3
#define STR_SHIFT               2
#define TYPE_SHIFT              0

#define PORT_MASK               0xFFFF0000
#define ADDR_MASK               (7 << ADDR_SHIFT)
#define SIZE_MASK               (7 << SIZE_SHIFT)
#define REP_MASK                (1 << REP_SHIFT)
#define STR_MASK                (1 << STR_SHIFT)
#define TYPE_MASK               (1 << TYPE_SHIFT)
/* End Definitions for Port IO */

#define PMIO_PAGE_OFFSET        1

#define	IOPM_SIZE	(8*1024 + 1) /* TODO: ensure that this need not be 12Kbtes, not just 8Kb+1 */
#define	MSRPM_SIZE	(8*1024)

/* fkvm data */

static int fkvm_loaded = 0;

static void *iopm = NULL; /* Should I allocate a vm_object_t instead? */
static void *msrpm = NULL; /* Should I allocate a vm_object_t instead? */

static void *hsave_area = NULL;

static eventhandler_tag exit_tag;

/* per-guest data */

enum {
	VCPU_REGS_RAX = 0,
	VCPU_REGS_RCX = 1,
	VCPU_REGS_RDX = 2,
	VCPU_REGS_RBX = 3,
	VCPU_REGS_RSP = 4,
	VCPU_REGS_RBP = 5,
	VCPU_REGS_RSI = 6,
	VCPU_REGS_RDI = 7,
	VCPU_REGS_R8 = 8,
	VCPU_REGS_R9 = 9,
	VCPU_REGS_R10 = 10,
	VCPU_REGS_R11 = 11,
	VCPU_REGS_R12 = 12,
	VCPU_REGS_R13 = 13,
	VCPU_REGS_R14 = 14,
	VCPU_REGS_R15 = 15,
	VCPU_REGS_RIP,
	NR_VCPU_REGS
};

struct vcpu {
	/* VCPU data */
	struct vmcb *vmcb;
	unsigned long vmcb_pa;

	unsigned long regs[NR_VCPU_REGS];
	unsigned long dr0;
	unsigned long dr1;
	unsigned long dr2;
	unsigned long dr3;
	u_int64_t host_fs_base;
	u_int64_t host_gs_base;

	struct {
		uint64_t default_type;
		uint64_t mtrr64k[MTRR_N64K/8];
		uint64_t mtrr16k[MTRR_N16K/8];
		uint64_t mtrr4k [MTRR_N4K /8];
#define FKVM_MTRR_NVAR 8
		uint64_t mtrrvar[FKVM_MTRR_NVAR *2];
	} mtrrs;

	struct guestvm *guest_vm;

	unsigned long virqs[256 / (sizeof(unsigned long) * 8)];
};

struct guestvm {
	struct vcpu *vcpus[MAX_VCPUS];
	int nr_vcpus;

	struct vmspace *sp;
	u_int64_t nested_cr3;

};

#define SVM_VMLOAD ".byte 0x0f, 0x01, 0xda"
#define SVM_VMRUN  ".byte 0x0f, 0x01, 0xd8"
#define SVM_VMSAVE ".byte 0x0f, 0x01, 0xdb"
#define SVM_CLGI   ".byte 0x0f, 0x01, 0xdd"
#define SVM_STGI   ".byte 0x0f, 0x01, 0xdc"
#define SVM_INVLPGA ".byte 0x0f, 0x01, 0xdf"

static void
fkvm_virq_dequeue(struct vcpu *vcpu);

static inline struct vcpu *
TD_GET_VCPU(struct thread *td)
{
	struct vcpu *vcpu;
	vcpu = td->vcpu;
	if (vcpu == NULL)
		printf("TD_GET_VCPU -> NULL\n");
	return vcpu;
}

static inline void
TD_SET_VCPU(struct thread *td, struct vcpu *vcpu)
{
	td->vcpu = vcpu;
}

static inline struct guestvm *
PROC_GET_GUESTVM(struct proc *proc)
{
	struct guestvm *guestvm;
	guestvm = proc->p_guestvm;
	return guestvm;
}

static inline void
PROC_SET_GUESTVM(struct proc *proc, struct guestvm *guestvm)
{
	proc->p_guestvm = guestvm;                                \
}

static void
print_vmcb_seg(struct vmcb_seg* vmcb_seg, const char* name)
{
	printf("%s Selector\n", name);
	printf("Selector                 : %" PRIx16 "\n", vmcb_seg->selector);
	printf("Attributes               : %" PRIx16 "\n", vmcb_seg->attrib);
	printf("Limit                    : %" PRIx32 "\n", vmcb_seg->limit);
	printf("Base Address             : %" PRIx64 "\n", vmcb_seg->base);
	printf("\n");
}

static void
print_vmcb(struct vmcb *vmcb)
{
	printf("VMCB Control Area\n");
	printf("Intercept CR Reads       : %" PRIx16 "\n", vmcb->control.intercept_cr_reads);
	printf("Intercept CR Writes      : %" PRIx16 "\n", vmcb->control.intercept_cr_writes);
	printf("Intercept DR Reads       : %" PRIx16 "\n", vmcb->control.intercept_dr_reads);
	printf("Intercept DR Writes      : %" PRIx16 "\n", vmcb->control.intercept_dr_writes);
	printf("Intercept Exceptions     : %" PRIx32 "\n", vmcb->control.intercept_exceptions);
	printf("Intercepts               : %" PRIx64 "\n", vmcb->control.intercepts);
	printf("Reserved 1: \n");
	for(int i=0; i < 44; i++) {
		printf("%" PRIx8 "", vmcb->control.reserved_1[i]); /* Should be Zero */
	}
	printf("\n");
	printf("IOPM Base PA             : %" PRIx64 "\n", vmcb->control.iopm_base_pa);
	printf("MSRPM Base PA            : %" PRIx64 "\n", vmcb->control.msrpm_base_pa);
	printf("TSC Offset               : %" PRIx64 "\n", vmcb->control.tsc_offset);
	printf("Guest ASID               : %" PRIx32 "\n", vmcb->control.guest_asid);
	printf("TLB Control              : %" PRIx8 "\n", vmcb->control.tlb_control);
	printf("Reserved 2               : \n");
	for(int i=0; i < 3; i++) {
		printf("%" PRIx8 "", vmcb->control.reserved_1[i]); /* Should be Zero */
	}
	printf("\n");
	printf("Virtual TPR              : %" PRIx8 "\n", vmcb->control.v_tpr);
	printf("Virtual IRQ Pending      : %" PRIx8 "\n", vmcb->control.v_irq_pending);
	printf("Virtual Interrupt        : %" PRIx8 "\n", vmcb->control.v_intr);
	printf("Virtual Interrupt Masking: %" PRIx8 "\n", vmcb->control.v_intr_masking);
	printf("Virtual Interrupt Vector : %" PRIx8 "\n", vmcb->control.v_intr_vector);
	printf("Reserved 6               : \n");
	for(int i=0; i < 3; i++) {
		printf("%" PRIx8 "", vmcb->control.reserved_6[i]); /* Should be Zero */
	}
	printf("\n");
	printf("Interrupt Shadow         : %" PRIx8 "\n", vmcb->control.intr_shadow);
	printf("Reserved 7               : \n");
	for(int i=0; i < 7; i++) {
		printf("%" PRIx8 "", vmcb->control.reserved_7[i]); /* Should be Zero */
	}
	printf("\n");
	printf("Exit Code                : %" PRIx64 "\n", vmcb->control.exit_code);
	printf("Exit Info 1              : %" PRIx64 "\n", vmcb->control.exit_info_1);
	printf("Exit Info 2              : %" PRIx64 "\n", vmcb->control.exit_info_2);
	printf("Exit Interrupt Info      : %" PRIx32 "\n", vmcb->control.exit_int_info);
	printf("Exit Interrupt Info Err Code: %" PRIx32 "\n", vmcb->control.exit_int_info_err_code);
	printf("Nested Control           : %" PRIx64 "\n", vmcb->control.nested_ctl);
	printf("Reserved 8               : \n");
	for(int i=0; i < 16; i++) {
		printf("%" PRIx8 "", vmcb->control.reserved_8[i]); /* Should be Zero */
	}
	printf("\n");
	printf("Event Injection          : %" PRIx64 "\n", vmcb->control.event_inj);
	printf("Nested CR3               : %" PRIx64 "\n", vmcb->control.nested_cr3);
	printf("LBR Virtualization Enable: %" PRIx64 "\n", vmcb->control.lbr_virt_enable);
	printf("Reserved 9               : \n");
	for(int i=0; i < 832; i++) {
		printf("%" PRIx8 "", vmcb->control.reserved_9[i]); /* Should be Zero */
	}
	printf("\n");

	printf("\n");

	printf("VMCB Save Area\n");
	print_vmcb_seg(&(vmcb->save.es), "ES");
	print_vmcb_seg(&(vmcb->save.cs), "CS");
	print_vmcb_seg(&(vmcb->save.ss), "SS");
	print_vmcb_seg(&(vmcb->save.ds), "DS");
	print_vmcb_seg(&(vmcb->save.fs), "FS");
	print_vmcb_seg(&(vmcb->save.gs), "GS");
	print_vmcb_seg(&(vmcb->save.gdtr), "GDTR");
	print_vmcb_seg(&(vmcb->save.ldtr), "LDTR");
	print_vmcb_seg(&(vmcb->save.idtr), "IDTR");
	print_vmcb_seg(&(vmcb->save.tr), "TR");
	printf("Reserved 1                : \n");
	for(int i=0; i < 43; i++) {
		printf("%" PRIx8 "", vmcb->save.reserved_1[i]); /* Should be Zero */
	}
	printf("\n");
	printf("Current Processor Level   : %" PRIx8 "\n", vmcb->save.cpl);
	printf("Reserved 2                : \n");
	for(int i=0; i < 4; i++) {
		printf("%" PRIx8 "", vmcb->save.reserved_2[i]); /* Should be Zero */
	}
	printf("\n");
	printf("EFER                      : %" PRIx64 "\n", vmcb->save.efer);
	printf("Reserved 3                : \n");
	for(int i=0; i < 112; i++) {
		printf("%" PRIx8 "", vmcb->save.reserved_3[i]); /* Should be Zero */
	}
	printf("\n");
	printf("Control Register 4        : %" PRIx64 "\n", vmcb->save.cr4);
	printf("Control Register 3        : %" PRIx64 "\n", vmcb->save.cr3);
	printf("Control Register 0        : %" PRIx64 "\n", vmcb->save.cr0);
	printf("Debug Register 7          : %" PRIx64 "\n", vmcb->save.dr7);
	printf("Debug Register 6          : %" PRIx64 "\n", vmcb->save.dr6);
	printf("RFlags                    : %" PRIx64 "\n", vmcb->save.rflags);
	printf("RIP                       : %" PRIx64 "\n", vmcb->save.rip);
	printf("Reserved 4                : \n");
	for(int i=0; i < 88; i++) {
		printf("%" PRIx8 "", vmcb->save.reserved_4[i]); /* Should be Zero */
	}
	printf("\n");
	printf("RSP                       : %" PRIx64 "\n", vmcb->save.rsp);
	printf("Reserved 5                : \n");
	for(int i=0; i < 24; i++) {
		printf("%" PRIx8 "", vmcb->save.reserved_5[i]); /* Should be Zero */
	}
	printf("\n");
	printf("RAX                       : %" PRIx64 "\n", vmcb->save.rax);
	printf("STAR                      : %" PRIx64 "\n", vmcb->save.star);
	printf("LSTAR                     : %" PRIx64 "\n", vmcb->save.lstar);
	printf("CSTAR                     : %" PRIx64 "\n", vmcb->save.cstar);
	printf("SFMASK                    : %" PRIx64 "\n", vmcb->save.sfmask);
	printf("Kernel GS Base            : %" PRIx64 "\n", vmcb->save.kernel_gs_base);
	printf("SYSENTER CS               : %" PRIx64 "\n", vmcb->save.sysenter_cs);
	printf("SYSENTER ESP              : %" PRIx64 "\n", vmcb->save.sysenter_esp);
	printf("SYSENTER EIP              : %" PRIx64 "\n", vmcb->save.sysenter_eip);
	printf("Control Register 2        : %" PRIx64 "\n", vmcb->save.cr2);
	printf("Reserved 6                : \n");
	for(int i=0; i < 32; i++) {
		printf("%" PRIx8 "", vmcb->save.reserved_6[i]); /* Should be Zero */
	}
	printf("\n");
	printf("Global PAT                : %" PRIx64 "\n", vmcb->save.g_pat);
	printf("Debug Control             : %" PRIx64 "\n", vmcb->save.dbg_ctl);
	printf("BR From                   : %" PRIx64 "\n", vmcb->save.br_from);
	printf("BR To                     : %" PRIx64 "\n", vmcb->save.br_to);
	printf("Last Exception From       : %" PRIx64 "\n", vmcb->save.last_excp_from);
	printf("Last Exception To         : %" PRIx64 "\n", vmcb->save.last_excp_to);

	printf("\n\n");
}

#if 0
static void
print_tss_desc(struct system_segment_descriptor *tss_desc)
{
	printf("TSS desc @ %p:\n", tss_desc);
	printf("sd_lolimit: 0x%" PRIx64 "\n", (u_int64_t) tss_desc->sd_lolimit);
	printf("sd_lobase:  0x%" PRIx64 "\n", (u_int64_t) tss_desc->sd_lobase);
	printf("sd_type:    0x%" PRIx64 "\n", (u_int64_t) tss_desc->sd_type);
	printf("sd_dpl:     0x%" PRIx64 "\n", (u_int64_t) tss_desc->sd_dpl);
	printf("sd_p:       0x%" PRIx64 "\n", (u_int64_t) tss_desc->sd_p);
	printf("sd_hilimit: 0x%" PRIx64 "\n", (u_int64_t) tss_desc->sd_hilimit);
	printf("sd_xx0:     0x%" PRIx64 "\n", (u_int64_t) tss_desc->sd_xx0);
	printf("sd_gran:    0x%" PRIx64 "\n", (u_int64_t) tss_desc->sd_gran);
	printf("sd_hibase:  0x%" PRIx64 "\n", (u_int64_t) tss_desc->sd_hibase);
	printf("sd_xx1:     0x%" PRIx64 "\n", (u_int64_t) tss_desc->sd_xx1);
	printf("sd_mbz:     0x%" PRIx64 "\n", (u_int64_t) tss_desc->sd_mbz);
	printf("sd_xx2:     0x%" PRIx64 "\n", (u_int64_t) tss_desc->sd_xx2);
	printf("\n\n");
}

static void
print_tss(struct system_segment_descriptor *tss_desc)
{
	u_int32_t *base;
	int limit;
	int i;

	base  = (u_int32_t*) ((((u_int64_t) tss_desc->sd_hibase)  << 24) | ((u_int64_t) tss_desc->sd_lobase));
	limit = ((tss_desc->sd_hilimit << 16) | tss_desc->sd_lolimit) / 4;

	printf("TSS: @ %p\n", base);
	for (i = 0; i <= limit; i++)
		printf("%x: 0x%" PRIx32 "\n", i, base[i]);
	printf("\n\n");
}
#endif

static inline void
print_vmcb_save_area(struct vmcb *vmcb)
{
	printf("VMCB save area:\n");
	printf("  cs:   [selector %" PRIx16 ", attrib %" PRIx16 ", limit %" PRIx32 ", base %" PRIx64 "]\n",
		vmcb->save.cs.selector,
		vmcb->save.cs.attrib,
		vmcb->save.cs.limit,
		vmcb->save.cs.base);
	printf("  fs:   [selector %" PRIx16 ", attrib %" PRIx16 ", limit %" PRIx32 ", base %" PRIx64 "]\n",
		vmcb->save.fs.selector,
		vmcb->save.fs.attrib,
		vmcb->save.fs.limit,
		vmcb->save.fs.base);
	printf("  gs:   [selector %" PRIx16 ", attrib %" PRIx16 ", limit %" PRIx32 ", base %" PRIx64 "]\n",
		vmcb->save.gs.selector,
		vmcb->save.gs.attrib,
		vmcb->save.gs.limit,
		vmcb->save.gs.base);
	printf("  tr:   [selector %" PRIx16 ", attrib %" PRIx16 ", limit %" PRIx32 ", base %" PRIx64 "]\n",
		vmcb->save.tr.selector,
		vmcb->save.tr.attrib,
		vmcb->save.tr.limit,
		vmcb->save.tr.base);
	printf("  ldtr:   [selector %" PRIx16 ", attrib %" PRIx16 ", limit %" PRIx32 ", base %" PRIx64 "]\n",
		vmcb->save.ldtr.selector,
		vmcb->save.ldtr.attrib,
		vmcb->save.ldtr.limit,
		vmcb->save.ldtr.base);
	printf("  rip: %" PRIx64 "\n", vmcb->save.rip);
	printf("  kernel_gs_base: %" PRIx64 "\n", vmcb->save.kernel_gs_base);
	printf("  star:   %" PRIx64 "\n", vmcb->save.star);
	printf("  lstar:  %" PRIx64 "\n", vmcb->save.lstar);
	printf("  cstar:  %" PRIx64 "\n", vmcb->save.cstar);
	printf("  sfmask: %" PRIx64 "\n", vmcb->save.sfmask);
	printf("  sysenter_cs:  %" PRIx64 "\n", vmcb->save.sysenter_cs);
	printf("  sysenter_esp: %" PRIx64 "\n", vmcb->save.sysenter_esp);
	printf("  sysenter_eip: %" PRIx64 "\n", vmcb->save.sysenter_eip);
	printf("\n\n");
}

static int
vmrun_assert(struct vmcb *vmcb)
{
#define A(cond) do { if ((cond)) { printf("Error: assertion not met on line %d\n", __LINE__); bad = 1; } } while (0)

	int bad;

	bad = 0;

	// The following are illegal:

	//EFER.SVME is zero.
	A((vmcb->save.efer   & 0x0000000000001000) == 0);

	// CR0.CD is zero and CR0.NW is set
	A( ((vmcb->save.cr0  & 0x0000000040000000) == 0) &&
	   ((vmcb->save.cr0  & 0x0000000020000000) != 0));

	// CR0[63:32] are not zero.
	A((vmcb->save.cr0  & 0xFFFFFFFF00000000) == 0xFFFFFFFF00000000);

	// Any MBZ bit of CR3 is set.
	A((vmcb->save.cr3  & 0xFFF0000000000000) != 0);

	// CR4[63:11] are not zero.
	A((vmcb->save.cr4  & 0xFFFFFFFFFFFFF800) == 0xFFFFFFFFFFFFF800);

	// DR6[63:32] are not zero.
	A((vmcb->save.dr6  & 0xFFFFFFFF00000000) == 0xFFFFFFFF00000000);

	// DR7[63:32] are not zero.
	A((vmcb->save.dr7  & 0xFFFFFFFF00000000) == 0xFFFFFFFF00000000);

	// EFER[63:15] are not zero.
	A((vmcb->save.efer & 0xFFFFFFFFFFFF8000) == 0xFFFFFFFFFFF8000);

	// EFER.LMA or EFER.LME is non-zero and this processor does not support long mode.
	//// A((vmcb->save.efer & 0x0000000000000500) != 0);

	// EFER.LME and CR0.PG are both set and CR4.PAE is zero.
	A( ((vmcb->save.efer & 0x0000000000000100) != 0) &&
	   ((vmcb->save.cr0  & 0x0000000080000000) != 0) &&
	   ((vmcb->save.cr4  & 0x0000000000000020) != 0));

	// EFER.LME and CR0.PG are both non-zero and CR0.PE is zero.
	A( ((vmcb->save.efer & 0x0000000000000100) != 0)  &&
	   ((vmcb->save.cr0  & 0x0000000080000000) != 0) &&
	   ((vmcb->save.cr0  & 0x0000000000000001) == 0));

	// EFER.LME, CR0.PG, CR4.PAE, CS.L, and CS.D are all non-zero.
	// cs.attrib = concat 55-52 and 47-40 (p372 v2)
	A( ((vmcb->save.efer & 0x0000000000000100) != 0)  &&
	   ((vmcb->save.cr0  & 0x0000000080000000) != 0) &&
	   ((vmcb->save.cr4  & 0x0000000000000020) != 0) &&
	   ((vmcb->save.cs.attrib & 0x0200) != 0)        &&
	   ((vmcb->save.cs.attrib & 0x0400) != 0));

	// The VMRUN intercept bit is clear.
	A((vmcb->control.intercepts & 0x0000000100000000) == 0);

	// The MSR or IOIO intercept tables extend to a physical address that is
	// greater than or equal to the maximum supported physical address.

	// Illegal event injection (see Section 15.19 on page 391).

	// ASID is equal to zero.
	A(vmcb->control.guest_asid == 0);

	// VMRUN can load a guest value of CR0 with PE = 0 but PG = 1, a
	// combination that is otherwise illegal (see Section 15.18).

	// In addition to consistency checks, VMRUN and #VMEXIT canonicalize (i.e.,
	// sign-extend to 63 bits) all base addresses in the segment registers
	// that have been loaded.

	return bad;

#undef A
}

static void
fkvm_vcpu_run(struct vcpu *vcpu)
{
	u_int64_t lstar;
	u_int64_t cstar;
	u_int64_t star;
	u_int64_t sfmask;

	u_short fs_selector;
	u_short gs_selector;
	u_short ldt_selector;

	unsigned long host_cr2;

	unsigned long host_dr0;
	unsigned long host_dr1;
	unsigned long host_dr2;
	unsigned long host_dr3;
	unsigned long host_dr6;
	unsigned long host_dr7;

	struct system_segment_descriptor *tss_desc;
	u_int64_t sel;

	struct vmcb *vmcb;

	//printf("begin fkvm_vcpu_run\n");

	vmcb = vcpu->vmcb;

	fkvm_virq_dequeue(vcpu);

	if (vmrun_assert(vmcb))
		return;

	tss_desc = (struct system_segment_descriptor*) (&gdt[GPROC0_SEL]);
	sel = GSEL(GPROC0_SEL, SEL_KPL);

//	printf("GSEL(GPROC0_SEL, SEL_KPL)=0x%" PRIx64 "\n", sel);
//	print_tss_desc(tss_desc);
//	print_tss(tss_desc);

//	print_vmcb_save_area(vmcb);
//	printf("vcpu->regs[VCPU_REGS_RIP]: 0x%lx\n", vcpu->regs[VCPU_REGS_RIP]);
//	disable_intr();

	vmcb->save.rax = vcpu->regs[VCPU_REGS_RAX];
	vmcb->save.rsp = vcpu->regs[VCPU_REGS_RSP];
	vmcb->save.rip = vcpu->regs[VCPU_REGS_RIP];

	/* meh: kvm has pre_svm_run(svm); */

	vcpu->host_fs_base = rdmsr(MSR_FSBASE);
	vcpu->host_gs_base = rdmsr(MSR_GSBASE);
//	printf("host_fs_base: 0x%" PRIx64 "\n", vcpu->host_fs_base);
//	printf("host_gs_base: 0x%" PRIx64 "\n", vcpu->host_gs_base);

	fs_selector = rfs();
	gs_selector = rgs();
	ldt_selector = rldt();
//	printf("fs selector: %hx\n", fs_selector);
//	printf("gs selector: %hx\n", gs_selector);
//	printf("ldt selector: %hx\n", ldt_selector);

	host_cr2 = rcr2();

	host_dr0 = rdr0();
	host_dr1 = rdr1();
	host_dr2 = rdr2();
	host_dr3 = rdr3();
	host_dr6 = rdr6();
	host_dr7 = rdr7();

	load_dr0(vcpu->dr0);
	load_dr1(vcpu->dr1);
	load_dr2(vcpu->dr2);
	load_dr3(vcpu->dr3);
	/* dr6 and dr7 are in the vmcb */

	// TODO: something with apic_base?

//	printf("MSR_STAR:   %" PRIx64 "\n", rdmsr(MSR_STAR));
//	printf("MSR_LSTAR:  %" PRIx64 "\n", rdmsr(MSR_LSTAR));
//	printf("MSR_CSTAR:  %" PRIx64 "\n", rdmsr(MSR_CSTAR));
//	printf("MSR_SF_MASK:  %" PRIx64 "\n", rdmsr(MSR_SF_MASK));

	star = rdmsr(MSR_STAR);
	lstar = rdmsr(MSR_LSTAR);
	cstar = rdmsr(MSR_CSTAR);
	sfmask = rdmsr(MSR_SF_MASK);

//	printf("CLGI...\n");

	__asm __volatile (SVM_CLGI);


//	enable_intr();

	__asm __volatile (
		"push %%rbp; \n\t"
		"mov %c[rbx](%[svm]), %%rbx \n\t"
		"mov %c[rcx](%[svm]), %%rcx \n\t"
		"mov %c[rdx](%[svm]), %%rdx \n\t"
		"mov %c[rsi](%[svm]), %%rsi \n\t"
		"mov %c[rdi](%[svm]), %%rdi \n\t"
		"mov %c[rbp](%[svm]), %%rbp \n\t"
		"mov %c[r8](%[svm]),  %%r8  \n\t"
		"mov %c[r9](%[svm]),  %%r9  \n\t"
		"mov %c[r10](%[svm]), %%r10 \n\t"
		"mov %c[r11](%[svm]), %%r11 \n\t"
		"mov %c[r12](%[svm]), %%r12 \n\t"
		"mov %c[r13](%[svm]), %%r13 \n\t"
		"mov %c[r14](%[svm]), %%r14 \n\t"
		"mov %c[r15](%[svm]), %%r15 \n\t"

		/* Enter guest mode */
		"push %%rax \n\t"
		"mov %c[vmcb](%[svm]), %%rax \n\t"
		SVM_VMLOAD "\n\t"
		SVM_VMRUN "\n\t"
		SVM_VMSAVE "\n\t"
		"pop %%rax \n\t"

		/* Save guest registers, load host registers */
		"mov %%rbx, %c[rbx](%[svm]) \n\t"
		"mov %%rcx, %c[rcx](%[svm]) \n\t"
		"mov %%rdx, %c[rdx](%[svm]) \n\t"
		"mov %%rsi, %c[rsi](%[svm]) \n\t"
		"mov %%rdi, %c[rdi](%[svm]) \n\t"
		"mov %%rbp, %c[rbp](%[svm]) \n\t"
		"mov %%r8,  %c[r8](%[svm]) \n\t"
		"mov %%r9,  %c[r9](%[svm]) \n\t"
		"mov %%r10, %c[r10](%[svm]) \n\t"
		"mov %%r11, %c[r11](%[svm]) \n\t"
		"mov %%r12, %c[r12](%[svm]) \n\t"
		"mov %%r13, %c[r13](%[svm]) \n\t"
		"mov %%r14, %c[r14](%[svm]) \n\t"
		"mov %%r15, %c[r15](%[svm]) \n\t"
		"pop %%rbp"
		:
		: [svm]"a"(vcpu),
		  [vmcb]"i"(offsetof(struct vcpu, vmcb_pa)),
		  [rbx]"i"(offsetof(struct vcpu, regs[VCPU_REGS_RBX])),
		  [rcx]"i"(offsetof(struct vcpu, regs[VCPU_REGS_RCX])),
		  [rdx]"i"(offsetof(struct vcpu, regs[VCPU_REGS_RDX])),
		  [rsi]"i"(offsetof(struct vcpu, regs[VCPU_REGS_RSI])),
		  [rdi]"i"(offsetof(struct vcpu, regs[VCPU_REGS_RDI])),
		  [rbp]"i"(offsetof(struct vcpu, regs[VCPU_REGS_RBP])),
		  [r8 ]"i"(offsetof(struct vcpu, regs[VCPU_REGS_R8 ])),
		  [r9 ]"i"(offsetof(struct vcpu, regs[VCPU_REGS_R9 ])),
		  [r10]"i"(offsetof(struct vcpu, regs[VCPU_REGS_R10])),
		  [r11]"i"(offsetof(struct vcpu, regs[VCPU_REGS_R11])),
		  [r12]"i"(offsetof(struct vcpu, regs[VCPU_REGS_R12])),
		  [r13]"i"(offsetof(struct vcpu, regs[VCPU_REGS_R13])),
		  [r14]"i"(offsetof(struct vcpu, regs[VCPU_REGS_R14])),
		  [r15]"i"(offsetof(struct vcpu, regs[VCPU_REGS_R15]))
		: "cc", "memory",
		"rbx", "rcx", "rdx", "rsi", "rdi",
		"r8", "r9", "r10", "r11" , "r12", "r13", "r14", "r15"
		);


	vcpu->regs[VCPU_REGS_RAX] = vmcb->save.rax;
	vcpu->regs[VCPU_REGS_RSP] = vmcb->save.rsp;
	vcpu->regs[VCPU_REGS_RIP] = vmcb->save.rip;

	vcpu->dr0 = rdr0();
	vcpu->dr1 = rdr1();
	vcpu->dr2 = rdr2();
	vcpu->dr3 = rdr3();
	/* dr6 and dr7 are in the vmcb */

	load_dr0(host_dr0);
	load_dr1(host_dr1);
	load_dr2(host_dr2);
	load_dr3(host_dr3);
	load_dr6(host_dr6);
	load_dr7(host_dr7);

	load_cr2(host_cr2);

	load_fs(fs_selector);
	load_gs(gs_selector);
	lldt(ldt_selector);

	wrmsr(MSR_FSBASE, vcpu->host_fs_base);
	wrmsr(MSR_GSBASE, vcpu->host_gs_base);

	tss_desc->sd_type = SDT_SYSTSS;
	ltr(sel);

	wrmsr(MSR_STAR, star);
	wrmsr(MSR_LSTAR, lstar);
	wrmsr(MSR_CSTAR, cstar);
	wrmsr(MSR_SF_MASK, sfmask);

//	disable_intr();

	__asm __volatile (SVM_STGI);

//	printf("STGI\n");

//	print_tss_desc(tss_desc);
//	print_tss(tss_desc);

//	print_vmcb_save_area(vmcb);

//	enable_intr();

	/* meh: next_rip */
}

static void
_fkvm_init_seg(struct vmcb_seg *seg, uint16_t attrib)
{
	seg->selector = 0;
	seg->attrib = VMCB_SELECTOR_P_MASK | attrib;
	seg->limit = 0xffff;
	seg->base = 0;
}

static inline void
fkvm_init_seg(struct vmcb_seg *seg)
{
	_fkvm_init_seg(seg, VMCB_SELECTOR_S_MASK | VMCB_SELECTOR_WRITE_MASK);
}

static inline void
fkvm_init_sys_seg(struct vmcb_seg *seg, uint16_t attrib)
{
	_fkvm_init_seg(seg, attrib);
}

static void*
fkvm_iopm_alloc(void)
{
	return contigmalloc(IOPM_SIZE, M_DEVBUF, 0, 0, -1UL, PAGE_SIZE, 0);
}

static void
fkvm_iopm_init(void *iopm)
{
	memset(iopm, 0xff, IOPM_SIZE); /* TODO: we may want to allow access to PC debug port */
}

static void
fkvm_iopm_free(void *iopm)
{
	contigfree(iopm, IOPM_SIZE, M_DEVBUF);
}

static void*
fkvm_msrpm_alloc(void)
{
	return contigmalloc(MSRPM_SIZE, M_DEVBUF, 0, 0, -1UL, PAGE_SIZE, 0);
}

static void
fkvm_msrpm_init(void *msrpm)
{
	memset(msrpm, 0xff, MSRPM_SIZE); /* TODO: we may want to allow some MSR accesses */
}

static void
fkvm_msrpm_free(void *msrpm)
{
	contigfree(msrpm, MSRPM_SIZE, M_DEVBUF);
}

static void*
fkvm_hsave_area_alloc(void)
{
	return contigmalloc(PAGE_SIZE, M_DEVBUF, 0, 0, -1UL, PAGE_SIZE, 0);
}

static void
fkvm_hsave_area_init(void *hsave_area)
{
}

static void
fkvm_hsave_area_free(void *hsave_area)
{
	contigfree(hsave_area, PAGE_SIZE, M_DEVBUF);
}

static struct vmspace*
fkvm_make_vmspace(void)
{
	struct vmspace *sp;

	sp = vmspace_alloc(0, 0xffffffffffffffff);
	if (sp == NULL) {
		printf("vmspace_alloc failed\n");
		return NULL;
	}

	return sp;
}

static void
fkvm_destroy_vmspace(struct vmspace* sp)
{
	vmspace_free(sp);
}

static struct vmcb*
fkvm_vmcb_alloc(void)
{
	return contigmalloc(PAGE_SIZE, M_DEVBUF, M_ZERO, 0, -1UL,
			PAGE_SIZE, 0);
}

static void
fkvm_vmcb_init(struct vmcb *vmcb)
{
	struct vmcb_control_area *control = &vmcb->control;
	struct vmcb_save_area *save = &vmcb->save;

	control->intercept_cr_reads =	INTERCEPT_CR0_MASK |
					INTERCEPT_CR3_MASK |
					INTERCEPT_CR4_MASK;

	control->intercept_cr_writes =	INTERCEPT_CR0_MASK |
					INTERCEPT_CR3_MASK |
					INTERCEPT_CR4_MASK |
					INTERCEPT_CR8_MASK;

#if 0
	control->intercept_dr_reads =	INTERCEPT_DR0_MASK |
					INTERCEPT_DR1_MASK |
					INTERCEPT_DR2_MASK |
					INTERCEPT_DR3_MASK;

	control->intercept_dr_writes =	INTERCEPT_DR0_MASK |
					INTERCEPT_DR1_MASK |
					INTERCEPT_DR2_MASK |
					INTERCEPT_DR3_MASK |
					INTERCEPT_DR5_MASK |
					INTERCEPT_DR7_MASK;
#else
	control->intercept_dr_reads  = 0;
	control->intercept_dr_writes = 0;
#endif

	control->intercept_exceptions = (1 << IDT_UD) | // Invalid Opcode
					(1 << IDT_MC);  // Machine Check

	control->intercepts =	INTERCEPT_INTR |
				INTERCEPT_NMI |
				INTERCEPT_SMI |
				INTERCEPT_CPUID |
				INTERCEPT_INVD |
				INTERCEPT_HLT |
				INTERCEPT_INVLPGA |
				INTERCEPT_IOIO_PROT |
				INTERCEPT_MSR_PROT |
				INTERCEPT_SHUTDOWN |
				INTERCEPT_VMRUN |
				INTERCEPT_VMMCALL |
				INTERCEPT_VMLOAD |
				INTERCEPT_VMSAVE |
				INTERCEPT_STGI |
				INTERCEPT_CLGI |
				INTERCEPT_SKINIT |
				INTERCEPT_WBINVD |
				INTERCEPT_MONITOR |
				INTERCEPT_MWAIT_UNCOND;

	control->iopm_base_pa = vtophys(iopm);
	control->msrpm_base_pa = vtophys(msrpm);
	control->tsc_offset = 0;

	/* TODO: remove this once we assign asid's to distinct VM's */
	control->guest_asid = 1;
	control->tlb_control = VMCB_TLB_CONTROL_FLUSH_ALL;

	/* let v_tpr default to 0 */
	/* let v_irq_pending default to 0 */
	/* let v_intr default to 0 */

	control->v_intr_masking = 1;

	/* let v_intr_vector default to 0 */
	/* let intr_shadow default to 0 */
	/* let exit_code, exit_info_1, exit_info_2, exit_int_info,
           exit_int_info_err_code default to 0 */

	control->nested_ctl = 1;

	/* let event_inj default to 0 */

	// (nested_cr3 is later)

	/* let lbr_virt_enable default to 0 */


	fkvm_init_seg(&save->ds);
	fkvm_init_seg(&save->es);
	fkvm_init_seg(&save->fs);
	fkvm_init_seg(&save->gs);
	fkvm_init_seg(&save->ss);

	_fkvm_init_seg(&save->cs, VMCB_SELECTOR_READ_MASK | VMCB_SELECTOR_S_MASK |
				  VMCB_SELECTOR_CODE_MASK);
	save->cs.selector = 0xf000;
	save->cs.base = 0xffff0000;

	save->gdtr.limit = 0xffff;
	save->idtr.limit = 0xffff;

	fkvm_init_sys_seg(&save->ldtr, SDT_SYSLDT);
	fkvm_init_sys_seg(&save->tr, SDT_SYS286BSY);

	save->g_pat = PAT_VALUE(PAT_WRITE_BACK, 0) | PAT_VALUE(PAT_WRITE_THROUGH, 1) |
		      PAT_VALUE(PAT_UNCACHED, 2) | PAT_VALUE(PAT_UNCACHEABLE, 3) |
		      PAT_VALUE(PAT_WRITE_BACK, 4) | PAT_VALUE(PAT_WRITE_THROUGH, 5) |
		      PAT_VALUE(PAT_UNCACHED, 6) | PAT_VALUE(PAT_UNCACHEABLE, 7);

	/* CR0 = 6000_0010h at boot */
	save->cr0 = CR0_ET | CR0_NW | CR0_CD;
	save->dr6 = 0xffff0ff0;
	save->dr7 = 0x400;
	save->rflags = 2;
	save->rip = 0x0000fff0;

	save->efer = EFER_SVME;
}

static void
fkvm_vmcb_free(struct vmcb *vmcb)
{
	contigfree(vmcb, PAGE_SIZE, M_DEVBUF);
}

static void
fkvm_virq_set(struct vcpu *vcpu, int virq)
{
	int i, j;

	i = virq / (sizeof(vcpu->virqs[0]) * 8);
	j = virq % (sizeof(vcpu->virqs[0]) * 8);

	vcpu->virqs[i] |= 1UL << j;
}

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))
#endif

static int
fkvm_virq_pop(struct vcpu *vcpu)
{
	int i, j;
	for (i = ARRAY_SIZE(vcpu->virqs) - 1; i >= 0; i--) {
		j = flsl(vcpu->virqs[i]);
		// virqs[i] == 0          =>  j = 0
		// virqs[i] == (1 <<  0)  =>  j = 1
		// ...
		if (j > 0) {
			vcpu->virqs[i] &= ~(1UL << (j - 1));
			return i * sizeof(vcpu->virqs[0]) * 8 + (j - 1);
		}
	}
	return -1;
}

#if 0
static void
fkvm_virq_test(struct vcpu *vcpu)
{
#define VIRQ_ASSERT(cond) do { \
	if (!(cond)) { \
		printf("irq test failed %d\n", __LINE__); \
	} \
} while (0)

	VIRQ_ASSERT(fkvm_virq_pop(vcpu) == -1);

	fkvm_virq_set(vcpu, 0);
	VIRQ_ASSERT(fkvm_virq_pop(vcpu) == 0);
	VIRQ_ASSERT(fkvm_virq_pop(vcpu) == -1);

	fkvm_virq_set(vcpu, 1);
	fkvm_virq_set(vcpu, 0);
	VIRQ_ASSERT(fkvm_virq_pop(vcpu) == 1);
	VIRQ_ASSERT(fkvm_virq_pop(vcpu) == 0);
	VIRQ_ASSERT(fkvm_virq_pop(vcpu) == -1);

	fkvm_virq_set(vcpu, 0);
	fkvm_virq_set(vcpu, 1);
	VIRQ_ASSERT(fkvm_virq_pop(vcpu) == 1);
	VIRQ_ASSERT(fkvm_virq_pop(vcpu) == 0);
	VIRQ_ASSERT(fkvm_virq_pop(vcpu) == -1);

	fkvm_virq_set(vcpu, 255);
	fkvm_virq_set(vcpu, 0);
	VIRQ_ASSERT(fkvm_virq_pop(vcpu) == 255);
	VIRQ_ASSERT(fkvm_virq_pop(vcpu) == 0);
	VIRQ_ASSERT(fkvm_virq_pop(vcpu) == -1);

	fkvm_virq_set(vcpu, 0);
	fkvm_virq_set(vcpu, 237);
	fkvm_virq_set(vcpu, 65);
	fkvm_virq_set(vcpu, 204);
	fkvm_virq_set(vcpu, 26);
	fkvm_virq_set(vcpu, 234);
	fkvm_virq_set(vcpu, 38);
	fkvm_virq_set(vcpu, 189);
	fkvm_virq_set(vcpu, 152);
	fkvm_virq_set(vcpu, 29);
	fkvm_virq_set(vcpu, 78);
	fkvm_virq_set(vcpu, 22);
	fkvm_virq_set(vcpu, 238);
	fkvm_virq_set(vcpu, 118);
	fkvm_virq_set(vcpu, 87);
	fkvm_virq_set(vcpu, 147);
	fkvm_virq_set(vcpu, 188);
	fkvm_virq_set(vcpu, 252);
	fkvm_virq_set(vcpu, 154);
	fkvm_virq_set(vcpu, 242);
	fkvm_virq_set(vcpu, 246);
	fkvm_virq_set(vcpu, 40);
	fkvm_virq_set(vcpu, 238);
	fkvm_virq_set(vcpu, 172);
	fkvm_virq_set(vcpu, 61);

	VIRQ_ASSERT(fkvm_virq_pop(vcpu) == 252);
	VIRQ_ASSERT(fkvm_virq_pop(vcpu) == 246);
	VIRQ_ASSERT(fkvm_virq_pop(vcpu) == 242);
	VIRQ_ASSERT(fkvm_virq_pop(vcpu) == 238);
	VIRQ_ASSERT(fkvm_virq_pop(vcpu) == 237);
	VIRQ_ASSERT(fkvm_virq_pop(vcpu) == 234);
	VIRQ_ASSERT(fkvm_virq_pop(vcpu) == 204);
	VIRQ_ASSERT(fkvm_virq_pop(vcpu) == 189);
	VIRQ_ASSERT(fkvm_virq_pop(vcpu) == 188);
	VIRQ_ASSERT(fkvm_virq_pop(vcpu) == 172);
	VIRQ_ASSERT(fkvm_virq_pop(vcpu) == 154);
	VIRQ_ASSERT(fkvm_virq_pop(vcpu) == 152);
	VIRQ_ASSERT(fkvm_virq_pop(vcpu) == 147);
	VIRQ_ASSERT(fkvm_virq_pop(vcpu) == 118);
	VIRQ_ASSERT(fkvm_virq_pop(vcpu) == 87);
	VIRQ_ASSERT(fkvm_virq_pop(vcpu) == 78);
	VIRQ_ASSERT(fkvm_virq_pop(vcpu) == 65);
	VIRQ_ASSERT(fkvm_virq_pop(vcpu) == 61);
	VIRQ_ASSERT(fkvm_virq_pop(vcpu) == 40);
	VIRQ_ASSERT(fkvm_virq_pop(vcpu) == 38);
	VIRQ_ASSERT(fkvm_virq_pop(vcpu) == 29);
	VIRQ_ASSERT(fkvm_virq_pop(vcpu) == 26);
	VIRQ_ASSERT(fkvm_virq_pop(vcpu) == 22);
	VIRQ_ASSERT(fkvm_virq_pop(vcpu) == 0);
	VIRQ_ASSERT(fkvm_virq_pop(vcpu) == -1);

}
#endif

static void
_fkvm_vmcb_set_virq(struct vcpu *vcpu, int virq)
{
	struct vmcb_control_area *control = &vcpu->vmcb->control;

	control->v_intr_vector = virq;
	control->v_intr        = 0xf;
	control->v_irq_pending = 1;
}

/* call this when we have a new interrupt for the vcpu */
static void
fkvm_virq_enqueue(struct vcpu *vcpu, int virq)
{
	struct vmcb_control_area *control = &vcpu->vmcb->control;

	if (control->v_irq_pending) {
		if (virq < control->v_intr_vector)
			fkvm_virq_set(vcpu, virq);
		else {
			fkvm_virq_set(vcpu, control->v_intr_vector);
			_fkvm_vmcb_set_virq(vcpu, virq);
		}
	}
	else {
		_fkvm_vmcb_set_virq(vcpu, virq);
	}
}

/* call this when the vcpu has finished handling an interrupt */
static void
fkvm_virq_dequeue(struct vcpu *vcpu)
{
	struct vmcb_control_area *control = &vcpu->vmcb->control;
	int virq;

	if (control->v_irq_pending)
		return; /* there's already an interrupt pending */

	virq = fkvm_virq_pop(vcpu);
	if (virq < 0)
		return; /* no interrupts waiting */

	_fkvm_vmcb_set_virq(vcpu, virq);
}

int
fkvm_inject_virq(struct thread *td, struct fkvm_inject_virq_args *uap)
{
	struct vcpu *vcpu = TD_GET_VCPU(td);

	if (uap->virq < 0 || uap->virq > 255)
		return EINVAL;

	fkvm_virq_enqueue(vcpu, uap->virq);

	return 0;
}

static struct vcpu*
fkvm_vcpu_create(struct guestvm *guest_vm)
{
	struct vcpu *vcpu;
	vcpu = malloc(sizeof(struct vcpu), M_DEVBUF, M_WAITOK|M_ZERO);

	vcpu->vmcb = fkvm_vmcb_alloc();
	vcpu->vmcb_pa = vtophys(vcpu->vmcb);
	printf("vmcb = 0x%p\n", vcpu->vmcb);
	printf("vcpu->vmcb_pa = 0x%lx\n", vcpu->vmcb_pa);

	fkvm_vmcb_init(vcpu->vmcb);
	vcpu->vmcb->control.nested_cr3 = guest_vm->nested_cr3;
	vcpu->regs[VCPU_REGS_RIP] = vcpu->vmcb->save.rip;

	vcpu->guest_vm = guest_vm;

	return vcpu;
}

static void
fkvm_vcpu_destroy(struct vcpu *vcpu)
{
	fkvm_vmcb_free(vcpu->vmcb);
	free(vcpu, M_DEVBUF);
}

static struct guestvm*
fkvm_guestvm_alloc(void)
{
	return malloc(sizeof(struct guestvm), M_DEVBUF, M_WAITOK|M_ZERO);
}

static void
fkvm_guestvm_free(struct guestvm* guest_vm)
{
	free(guest_vm, M_DEVBUF);
}

static void
fkvm_guestvm_add_vcpu(struct guestvm *guest_vm, struct vcpu *vcpu)
{
	guest_vm->vcpus[guest_vm->nr_vcpus] = vcpu;
	guest_vm->nr_vcpus++;	/* TODO: Probably not safe to increment */
				/* How about a lock to protect all of this? */
}


int
fkvm_userpoke(struct thread *td, struct fkvm_userpoke_args *uap)
{
	printf("fkvm_userpoke\n");

	if (!fkvm_loaded)
		return ENODEV;

	return ENOSYS;
}

static int
fkvm_mem_has_entry(vm_map_entry_t expected_entry, vm_map_t vm_map, vm_offset_t vaddr)
{
	vm_map_entry_t lookup_entry;
	vm_object_t    throwaway_object;
	vm_pindex_t    throwaway_pindex;
	vm_prot_t      throwaway_prot;
	boolean_t      throwaway_wired;
	int error;

	error = vm_map_lookup(&vm_map, /* IN/OUT */
			      vaddr,
			      VM_PROT_READ|VM_PROT_WRITE,
			      &lookup_entry,	  /* OUT */
			      &throwaway_object,  /* OUT */
			      &throwaway_pindex,  /* OUT */
			      &throwaway_prot,	  /* OUT */
			      &throwaway_wired);  /* OUT */
	if (error != KERN_SUCCESS)
		return 0;
	vm_map_lookup_done(vm_map, lookup_entry);
	return (lookup_entry == expected_entry);
}

static int
fkvm_guest_check_range(struct guestvm *guest_vm, uint64_t start, uint64_t end)
{
	vm_map_t       guest_vm_map;
	vm_map_entry_t lookup_entry;
	vm_object_t    throwaway_object;
	vm_pindex_t    throwaway_pindex;
	vm_prot_t      throwaway_prot;
	boolean_t      throwaway_wired;
	int ret;
	int error;

	guest_vm_map = &guest_vm->sp->vm_map;

	error = vm_map_lookup(&guest_vm_map, /* IN/OUT */
			      start,
			      VM_PROT_READ|VM_PROT_WRITE,
			      &lookup_entry,	  /* OUT */
			      &throwaway_object,  /* OUT */
			      &throwaway_pindex,  /* OUT */
			      &throwaway_prot,	  /* OUT */
			      &throwaway_wired);  /* OUT */
	if (error != KERN_SUCCESS)
		return EFAULT;
	vm_map_lookup_done(guest_vm_map, lookup_entry);

	/*
	TODO: We can't actually nest the lookups:
	panic: _sx_xlock_hard: recursed on non-recursive sx user map @ ../../../vm/vm_map.c:3115
	Therefore, I've moved the lookup_done above for now, but we really need a lock here.

	Maybe it's better to use vm_map_lookup_entry directly.
	*/


	if (fkvm_mem_has_entry(lookup_entry, guest_vm_map, end))
		ret = 0;
	else
		ret = EFAULT;

	return ret;
}

static void
fkvm_get_regs_regs(struct vcpu *vcpu, struct kvm_regs *out)
{
	out->rax    = vcpu->regs[VCPU_REGS_RAX];
	out->rbx    = vcpu->regs[VCPU_REGS_RBX];
	out->rcx    = vcpu->regs[VCPU_REGS_RCX];
	out->rdx    = vcpu->regs[VCPU_REGS_RDX];
	out->rsi    = vcpu->regs[VCPU_REGS_RSI];
	out->rdi    = vcpu->regs[VCPU_REGS_RDI];
	out->rsp    = vcpu->regs[VCPU_REGS_RSP];
	out->rbp    = vcpu->regs[VCPU_REGS_RBP];
	out->r8     = vcpu->regs[VCPU_REGS_R8];
	out->r9     = vcpu->regs[VCPU_REGS_R9];
	out->r10    = vcpu->regs[VCPU_REGS_R10];
	out->r11    = vcpu->regs[VCPU_REGS_R11];
	out->r12    = vcpu->regs[VCPU_REGS_R12];
	out->r13    = vcpu->regs[VCPU_REGS_R13];
	out->r14    = vcpu->regs[VCPU_REGS_R14];
	out->r15    = vcpu->regs[VCPU_REGS_R15];
	out->rip    = vcpu->regs[VCPU_REGS_RIP];
	out->rflags = vcpu->vmcb->save.rflags;
}

static void
fkvm_set_regs_regs(struct vcpu *vcpu, const struct kvm_regs *in)
{
	vcpu->regs[VCPU_REGS_RAX] = in->rax;
	vcpu->regs[VCPU_REGS_RBX] = in->rbx;
	vcpu->regs[VCPU_REGS_RCX] = in->rcx;
	vcpu->regs[VCPU_REGS_RDX] = in->rdx;
	vcpu->regs[VCPU_REGS_RSI] = in->rsi;
	vcpu->regs[VCPU_REGS_RDI] = in->rdi;
	vcpu->regs[VCPU_REGS_RSP] = in->rsp;
	vcpu->regs[VCPU_REGS_RBP] = in->rbp;
	vcpu->regs[VCPU_REGS_R8]  = in->r8;
	vcpu->regs[VCPU_REGS_R9]  = in->r9;
	vcpu->regs[VCPU_REGS_R10] = in->r10;
	vcpu->regs[VCPU_REGS_R11] = in->r11;
	vcpu->regs[VCPU_REGS_R12] = in->r12;
	vcpu->regs[VCPU_REGS_R13] = in->r13;
	vcpu->regs[VCPU_REGS_R14] = in->r14;
	vcpu->regs[VCPU_REGS_R15] = in->r15;
	vcpu->regs[VCPU_REGS_RIP] = in->rip;
	vcpu->vmcb->save.rflags   = in->rflags;
}

static void
fkvm_get_vmcb_dtable(struct vmcb_seg *vmcb_seg, struct kvm_dtable *fkvm_dtable)
{
	fkvm_dtable->base = vmcb_seg->base;
	fkvm_dtable->limit = vmcb_seg->limit;
}

static void
fkvm_set_vmcb_dtable(struct vmcb_seg *vmcb_seg, struct kvm_dtable *fkvm_dtable)
{
	vmcb_seg->base = fkvm_dtable->base;
	vmcb_seg->limit = fkvm_dtable->limit;
}

static void
fkvm_get_vmcb_seg(struct vmcb_seg *vmcb_seg, struct kvm_segment *fkvm_seg)
{
	fkvm_seg->base = vmcb_seg->base;
	fkvm_seg->limit = vmcb_seg->limit;
	fkvm_seg->selector = vmcb_seg->selector;

	if (vmcb_seg->attrib == 0)
		fkvm_seg->unusable = 1;
	else {
		fkvm_seg->type    = (vmcb_seg->attrib & VMCB_SELECTOR_TYPE_MASK);
		fkvm_seg->s       = (vmcb_seg->attrib & VMCB_SELECTOR_S_MASK) >> VMCB_SELECTOR_S_SHIFT;
		fkvm_seg->dpl     = (vmcb_seg->attrib & VMCB_SELECTOR_DPL_MASK) >> VMCB_SELECTOR_DPL_SHIFT;
		fkvm_seg->present = (vmcb_seg->attrib & VMCB_SELECTOR_P_MASK) >> VMCB_SELECTOR_P_SHIFT;
		fkvm_seg->avl     = (vmcb_seg->attrib & VMCB_SELECTOR_AVL_MASK) >> VMCB_SELECTOR_AVL_SHIFT;
		fkvm_seg->l       = (vmcb_seg->attrib & VMCB_SELECTOR_L_MASK) >> VMCB_SELECTOR_L_SHIFT;
		fkvm_seg->db      = (vmcb_seg->attrib & VMCB_SELECTOR_DB_MASK) >> VMCB_SELECTOR_DB_SHIFT;
		fkvm_seg->g       = (vmcb_seg->attrib & VMCB_SELECTOR_G_MASK) >> VMCB_SELECTOR_G_SHIFT;
	}
}

static void
fkvm_set_vmcb_seg(struct vmcb_seg *vmcb_seg, struct kvm_segment *fkvm_seg)
{
	vmcb_seg->base = fkvm_seg->base;
	vmcb_seg->limit = fkvm_seg->limit;
	vmcb_seg->selector = fkvm_seg->selector;

	if (fkvm_seg->unusable)
		vmcb_seg->attrib=0;
	else {
		vmcb_seg->attrib = (fkvm_seg->type & VMCB_SELECTOR_TYPE_MASK);
		vmcb_seg->attrib |= (fkvm_seg->s & 1) << VMCB_SELECTOR_S_SHIFT;
		vmcb_seg->attrib |= (fkvm_seg->dpl & 3) << VMCB_SELECTOR_DPL_SHIFT;
		vmcb_seg->attrib |= (fkvm_seg->present & 1) << VMCB_SELECTOR_P_SHIFT;
		vmcb_seg->attrib |= (fkvm_seg->avl & 1) << VMCB_SELECTOR_AVL_SHIFT;
		vmcb_seg->attrib |= (fkvm_seg->l & 1) << VMCB_SELECTOR_L_SHIFT;
		vmcb_seg->attrib |= (fkvm_seg->db & 1) << VMCB_SELECTOR_DB_SHIFT;
		vmcb_seg->attrib |= (fkvm_seg->g & 1) << VMCB_SELECTOR_G_SHIFT;
	}
}

static uint64_t
fkvm_get_cr8(struct vcpu *vcpu)
{
	// TODO: if cr8 has reserved bits inject GP Fault, return

	return (uint64_t) vcpu->vmcb->control.v_tpr;
}

static void
fkvm_set_cr8(struct vcpu *vcpu, uint64_t cr8)
{
	// TODO: if cr8 has reserved bits inject GP Fault, return

	vcpu->vmcb->control.v_tpr = (uint8_t) cr8;
}

static uint64_t
fkvm_get_efer(struct vcpu *vcpu)
{
	struct vmcb *vmcb = vcpu->vmcb;

	return vmcb->save.efer & (~EFER_SVME);
}

static void
fkvm_set_efer(struct vcpu *vcpu, uint64_t efer)
{
	struct vmcb *vmcb = vcpu->vmcb;
	//TODO: if efer has reserved bits set: inject GP Fault

	if (vmcb->save.cr0 & CR0_PG) { //If paging is enabled do not allow changes to LME
		if ((vmcb->save.efer & EFER_LME) != (efer & EFER_LME)) {
			printf("fkvm_set_efer: attempt to change LME while paging\n");
			//TODO: inject GP fault
		}
	}
	
	vmcb->save.efer = efer | EFER_SVME;
}

static void
fkvm_get_regs_sregs(struct vcpu *vcpu, struct kvm_sregs *out)
{
	struct vmcb *vmcb = vcpu->vmcb;

	fkvm_get_vmcb_seg(&vmcb->save.cs, &out->cs);
	fkvm_get_vmcb_seg(&vmcb->save.ds, &out->ds);
	fkvm_get_vmcb_seg(&vmcb->save.es, &out->es);
	fkvm_get_vmcb_seg(&vmcb->save.fs, &out->fs);
	fkvm_get_vmcb_seg(&vmcb->save.gs, &out->gs);
	fkvm_get_vmcb_seg(&vmcb->save.ss, &out->ss);
	fkvm_get_vmcb_seg(&vmcb->save.tr, &out->tr);
	fkvm_get_vmcb_seg(&vmcb->save.ldtr, &out->ldt);

	fkvm_get_vmcb_dtable(&vmcb->save.idtr, &out->idt);
	fkvm_get_vmcb_dtable(&vmcb->save.gdtr, &out->gdt);

	out->cr0 = vmcb->save.cr0;
	out->cr2 = vmcb->save.cr2;
	out->cr3 = vmcb->save.cr3;
	out->cr4 = vmcb->save.cr4;

	out->cr8 = fkvm_get_cr8(vcpu);
	out->efer = fkvm_get_efer(vcpu);

	out->dr0 = vcpu->dr0;
	out->dr1 = vcpu->dr1;
	out->dr2 = vcpu->dr2;
	out->dr3 = vcpu->dr3;
	out->dr6 = vmcb->save.dr6;
	out->dr7 = vmcb->save.dr7;

	/* TODO: apic_base */
	/* TODO: irq_pending, interrupt_bitmap, irq_summary */
}

static void
fkvm_set_regs_sregs(struct vcpu *vcpu, struct kvm_sregs *in)
{
	struct vmcb *vmcb = vcpu->vmcb;

	fkvm_set_vmcb_seg(&vmcb->save.cs, &in->cs);
	fkvm_set_vmcb_seg(&vmcb->save.ds, &in->ds);
	fkvm_set_vmcb_seg(&vmcb->save.es, &in->es);
	fkvm_set_vmcb_seg(&vmcb->save.fs, &in->fs);
	fkvm_set_vmcb_seg(&vmcb->save.gs, &in->gs);
	fkvm_set_vmcb_seg(&vmcb->save.ss, &in->ss);
	fkvm_set_vmcb_seg(&vmcb->save.tr, &in->tr);
	fkvm_set_vmcb_seg(&vmcb->save.ldtr, &in->ldt);

	vmcb->save.cpl = (vmcb->save.cs.attrib >> VMCB_SELECTOR_DPL_SHIFT) & 3;

	fkvm_set_vmcb_dtable(&vmcb->save.idtr, &in->idt);
	fkvm_set_vmcb_dtable(&vmcb->save.gdtr, &in->gdt);

	vmcb->save.cr0 = in->cr0;
	vmcb->save.cr2 = in->cr2;
	vmcb->save.cr3 = in->cr3;
	vmcb->save.cr4 = in->cr4;

	fkvm_set_cr8(vcpu, in->cr8);
	fkvm_set_efer(vcpu, in->efer);

	vcpu->dr0 = in->dr0;
	vcpu->dr1 = in->dr1;
	vcpu->dr2 = in->dr2;
	vcpu->dr3 = in->dr3;
	vmcb->save.dr6 = in->dr6;
	vmcb->save.dr7 = in->dr7;

	/* TODO: apic_base */
	/* TODO: irq_pending, interrupt_bitmap, irq_summary */
}

static int
fkvm_get_reg_msr(struct vcpu *vcpu, uint32_t index, uint64_t *data) {
	struct vmcb *vmcb = vcpu->vmcb;

	switch(index) {

	case MSR_TSC: {
		uint64_t tsc;

		tsc = rdtsc();
		*data = vmcb->control.tsc_offset + tsc;
		break;
	}

	case MSR_STAR: {
		*data = vmcb->save.star;
		break;
	}

	case MSR_LSTAR: {
		*data = vmcb->save.lstar;
		break;
	}

	case MSR_CSTAR: {
		*data = vmcb->save.cstar;
		break;
	}

	case MSR_GSBASE: {
		*data = vmcb->save.kernel_gs_base;
		break;
	}

	case MSR_SF_MASK: {
		*data = vmcb->save.sfmask;
		break;
	}

	case MSR_SYSENTER_CS_MSR: {
		*data = vmcb->save.sysenter_cs;
		break;
	}

	case MSR_SYSENTER_EIP_MSR: {
		*data = vmcb->save.sysenter_eip;
		break;
	}

	case MSR_SYSENTER_ESP_MSR: {
		*data = vmcb->save.sysenter_esp;
		break;
	}

	case MSR_DEBUGCTLMSR: {
		printf("unimplemented at %d\n", __LINE__);
		return ENOSYS;
		break;
	}

	case MSR_PERFEVSEL0 ... MSR_PERFEVSEL3:
	case MSR_PERFCTR0 ... MSR_PERFCTR3: {
		printf("unimplemented at %d\n", __LINE__);
		return ENOSYS;
		break;
	}

	case MSR_EFER: {
		*data = fkvm_get_efer(vcpu);
		break;
	}

	case MSR_MCG_CAP:
	case MSR_MCG_STATUS:
	case MSR_MCG_CTL:
	case MSR_MC0_CTL ... MSR_MC5_MISC : {
		*data = 0;
		break;
	}

	//TODO: MSR_IA32_UCODE_REV
	//TODO: MSR_IA32_UCODE_WRITE

	case MSR_MTRRcap: {
		*data = MTRR_CAP_WC | MTRR_CAP_FIXED | FKVM_MTRR_NVAR;
		break;
	}

	case MSR_MTRRdefType: {
		*data = vcpu->mtrrs.default_type;
		break;
	}

	case MSR_MTRR64kBase ... (MSR_MTRR64kBase + MTRR_N64K - 1): {
		*data = vcpu->mtrrs.mtrr64k[index - MSR_MTRR64kBase];
		break;
	}

	case MSR_MTRR16kBase ... (MSR_MTRR16kBase + MTRR_N16K - 1): {
		*data = vcpu->mtrrs.mtrr16k[index - MSR_MTRR16kBase];
		break;
	}

	case MSR_MTRR4kBase  ... (MSR_MTRR4kBase  + MTRR_N4K  - 1): {
		*data = vcpu->mtrrs.mtrr4k[index - MSR_MTRR4kBase];
		break;
	}

	case MSR_MTRRVarBase ... (MSR_MTRRVarBase + FKVM_MTRR_NVAR * 2 - 1): {
		*data = vcpu->mtrrs.mtrrvar[index - MSR_MTRRVarBase];
		break;
	}

	case MSR_APICBASE: {
		printf("unimplemented at %d\n", __LINE__);
		return ENOSYS;
		break;
	}

	case MSR_IA32_MISC_ENABLE: {
		printf("unimplemented at %d\n", __LINE__);
		return ENOSYS;
		break;
	}

	//TODO: MSR_KVM_WALL_CLOCK
	//TODO: MSR_KVM_SYSTEM_TIME

	default:
		printf("Did not get unimplemented msr: 0x%" PRIx32 "\n", index);
		return ENOSYS;
	}

	return 0;
}

static void
fkvm_get_regs_msrs(struct vcpu *vcpu, uint32_t nmsrs, struct kvm_msr_entry *entries) {
	int i;

	for (i = 0; i < nmsrs; i++) {
		fkvm_get_reg_msr(vcpu, entries[i].index, &entries[i].data);
	}
}

static int
fkvm_set_reg_msr(struct vcpu *vcpu, uint32_t index, uint64_t data) {
	struct vmcb *vmcb = vcpu->vmcb;

	switch(index) {

	case MSR_TSC: {
		uint64_t tsc;

		tsc = rdtsc();
		vmcb->control.tsc_offset = data - tsc;
		break;
	}

	case MSR_STAR: {
		vmcb->save.star = data;
		break;
	}

	case MSR_LSTAR: {
		vmcb->save.lstar = data;
		break;
	}

	case MSR_CSTAR: {
		vmcb->save.cstar = data;
		break;
	}

	case MSR_GSBASE: {
		vmcb->save.kernel_gs_base = data;
		break;
	}

	case MSR_SF_MASK: {
		vmcb->save.sfmask = data;
		break;
	}

	case MSR_SYSENTER_CS_MSR: {
		vmcb->save.sysenter_cs = data;
		break;
	}

	case MSR_SYSENTER_EIP_MSR: {
		vmcb->save.sysenter_eip = data;
		break;
	}

	case MSR_SYSENTER_ESP_MSR: {
		vmcb->save.sysenter_esp = data;
		break;
	}

	case MSR_DEBUGCTLMSR: {
		printf("unimplemented at %d\n", __LINE__);
		return ENOSYS;
		break;
	}

	case MSR_PERFEVSEL0 ... MSR_PERFEVSEL3:
	case MSR_PERFCTR0 ... MSR_PERFCTR3: {
		printf("unimplemented at %d\n", __LINE__);
		return ENOSYS;
		break;
	}

	case MSR_EFER: {
		fkvm_set_efer(vcpu, data);
		break;
	}

	case MSR_MCG_CAP:
	case MSR_MCG_STATUS:
	case MSR_MCG_CTL:
	case MSR_MC0_CTL ... MSR_MC5_MISC : {
		/* ignore */
		break;
	}

	//TODO: MSR_IA32_UCODE_REV
	//TODO: MSR_IA32_UCODE_WRITE

	case MSR_MTRRdefType: {
		vcpu->mtrrs.default_type = data;
		break;
	}

	case MSR_MTRR64kBase ... (MSR_MTRR64kBase + MTRR_N64K - 1): {
		vcpu->mtrrs.mtrr64k[index - MSR_MTRR64kBase] = data;
		break;
	}

	case MSR_MTRR16kBase ... (MSR_MTRR16kBase + MTRR_N16K - 1): {
		vcpu->mtrrs.mtrr16k[index - MSR_MTRR16kBase] = data;
		break;
	}

	case MSR_MTRR4kBase  ... (MSR_MTRR4kBase  + MTRR_N4K  - 1): {
		vcpu->mtrrs.mtrr4k[index - MSR_MTRR4kBase] = data;
		break;
	}

	case MSR_MTRRVarBase ... (MSR_MTRRVarBase + FKVM_MTRR_NVAR * 2 - 1): {
		vcpu->mtrrs.mtrrvar[index - MSR_MTRRVarBase] = data;
		break;
	}

	case MSR_APICBASE: {
		printf("unimplemented at %d\n", __LINE__);
		return ENOSYS;
		break;
	}

	case MSR_IA32_MISC_ENABLE: {
		printf("unimplemented at %d\n", __LINE__);
		return ENOSYS;
		break;
	}

	//TODO: MSR_KVM_WALL_CLOCK
	//TODO: MSR_KVM_SYSTEM_TIME

	default:
		printf("Did not set unimplemented msr: 0x%" PRIx32 "\n", index);
		return ENOSYS;
	}

	return 0;
}

static void
fkvm_set_regs_msrs(struct vcpu *vcpu, uint32_t nmsrs, struct kvm_msr_entry *entries) {
	int i;

	for (i = 0; i < nmsrs; i++) {
		fkvm_set_reg_msr(vcpu, entries[i].index, entries[i].data);
	}
}

/* System Calls */

int
fkvm_get_regs(struct thread *td, struct fkvm_get_regs_args *uap)
{
	struct vcpu *vcpu;
	int error;

	if (!fkvm_loaded)
		return ENODEV;

	vcpu = TD_GET_VCPU(td);
	if (vcpu == NULL)
		return ENODEV;

	switch (uap->type) {

	case FKVM_REGS_TYPE_REGS: {
		struct kvm_regs out;
		fkvm_get_regs_regs(vcpu, &out);
		return copyout(&out, uap->regs, sizeof(out));
	}

	case FKVM_REGS_TYPE_SREGS: {
		struct kvm_sregs out;
		fkvm_get_regs_sregs(vcpu, &out);
		return copyout(&out, uap->regs, sizeof(out));
	}

	case FKVM_REGS_TYPE_MSRS: {
		struct kvm_msr_entry *user_entries;
		struct kvm_msr_entry *entries;
		int size;

		user_entries = (struct kvm_msr_entry *)uap->regs;

		size = sizeof(*entries) * uap->n;
		entries = malloc(size, M_DEVBUF, M_WAITOK|M_ZERO);
		if (entries == NULL)
			return ENOMEM;

		error = copyin(user_entries, entries, size);
		if (error != 0) {
			printf("FKVM_REGS_TYPE_MSRS: unable to copyin entries\n");
			free(entries, M_DEVBUF);
			return error;
		}

		fkvm_get_regs_msrs(vcpu, uap->n, entries);

		error = copyout(user_entries, entries, size);
		if (error != 0) {
			printf("FKVM_REGS_TYPE_MSRS: unable to copyout entries\n");
		}

		free(entries, M_DEVBUF);
		return error;
	}

	default:
		return EINVAL;
	}
}

int
fkvm_set_regs(struct thread *td, struct fkvm_set_regs_args *uap)
{
	struct vcpu *vcpu;
	int error = 0;

	vcpu = TD_GET_VCPU(td);
	if (vcpu == NULL)
		return ENODEV;

	switch (uap->type) {

	case FKVM_REGS_TYPE_REGS: {
		struct kvm_regs in;
		error = copyin(uap->regs, &in, sizeof(in));
		if (error != 0)
			return error;
		fkvm_set_regs_regs(vcpu, &in);
		return 0;
	}

	case FKVM_REGS_TYPE_SREGS: {
		struct kvm_sregs in;
		error = copyin(uap->regs, &in, sizeof(in));
		if (error != 0)
			return error;
		fkvm_set_regs_sregs(vcpu, &in);
		return 0;
	}

	case FKVM_REGS_TYPE_MSRS: {
		struct kvm_msr_entry *user_entries;
		struct kvm_msr_entry *entries;
		int size;

		user_entries = (struct kvm_msr_entry *)uap->regs;

		size = sizeof(*entries) * uap->n;
		entries = malloc(size, M_DEVBUF, M_WAITOK|M_ZERO);
		if (entries == NULL)
			return ENOMEM;

		error = copyin(user_entries, entries, size);
		if (error != 0) {
			printf("FKVM_REGS_TYPE_MSRS: unable to copyin entries\n");
			free(entries, M_DEVBUF);
			return error;
		}

		fkvm_set_regs_msrs(vcpu, uap->n, entries);

		free(entries, M_DEVBUF);
		return error;
	}

	default:
		return EINVAL;
	}
}

/* This function can only be called with multiples of page sizes */
/* vaddr as NULL overloads to fkvm_guest_check_range */
int
fkvm_set_user_mem_region(struct thread *td, struct fkvm_set_user_mem_region_args *uap)
{
	struct guestvm *guest_vm;

	vm_offset_t start;
	vm_offset_t end;

	struct vmspace *user_vm_space;
	vm_map_t user_vm_map;

	vm_object_t    vm_object;
	vm_pindex_t    vm_object_pindex;
	vm_ooffset_t   vm_object_offset;
	vm_prot_t      throwaway_prot;
	boolean_t      throwaway_wired;
	vm_map_entry_t lookup_entry;

	int error;

	guest_vm = PROC_GET_GUESTVM(td->td_proc);
	if (guest_vm == NULL) {
		printf("PROC_GET_GUESTVM -> NULL\n");
		return ENODEV;
	}

	start = uap->guest_pa;
	end = uap->guest_pa + uap->size - 1;
	printf("start: 0x%" PRIx64 " bytes\n", start);
	printf("end:   0x%" PRIx64 " bytes\n", end);

	if (uap->vaddr == 0)
		return fkvm_guest_check_range(guest_vm, start, end);

	user_vm_space = td->td_proc->p_vmspace;
	user_vm_map = &user_vm_space->vm_map;
	printf("user vm space: %p\n", user_vm_space);
	printf("user vm map:   %p\n", user_vm_map);

	error = vm_map_lookup(&user_vm_map, /* IN/OUT */
			      uap->vaddr,
			      VM_PROT_READ|VM_PROT_WRITE,
			      &lookup_entry,	  /* OUT */
			      &vm_object,	  /* OUT */
			      &vm_object_pindex,  /* OUT */
			      &throwaway_prot,	  /* OUT */
			      &throwaway_wired);  /* OUT */
	if (error != KERN_SUCCESS) {
		printf("vm_map_lookup failed: %d\n", error);
		return EFAULT;
	}

	/* TODO: Trust the user that the full region is valid.
	 * This is very bad. See the note in fkvm_guest_check_range
	 * on nesting vm lookups. */
#if 0
	if (!fkvm_mem_has_entry(lookup_entry, user_vm_map, uap->vaddr + uap->size)) {
		printf("end of range not contained in same vm map entry as start\n");
		return EFAULT;
	}
#endif

	printf("vm object: %p\n", vm_object);
	printf("  size: %d pages\n", (int) vm_object->size);

	vm_object_offset = IDX_TO_OFF(vm_object_pindex);
	printf("vm_ooffset: 0x%" PRIx64 "\n", vm_object_offset);

	vm_object_reference(vm_object); // TODO: this might be a mem leak

	vm_map_lookup_done(user_vm_map, lookup_entry);

	error = vm_map_insert(&guest_vm->sp->vm_map,
			      vm_object,
			      vm_object_offset,
			      start,
			      end,
			      VM_PROT_ALL, VM_PROT_ALL,
			      0);
	if (error != KERN_SUCCESS) {
		printf("vm_map_insert failed: %d\n", error);
		switch (error) {
		case KERN_INVALID_ADDRESS:
			return EINVAL;
		case KERN_NO_SPACE:
			return ENOMEM;
		default:
			return 1;
		}
	}

	return 0;
}

int
fkvm_unset_user_mem_region(struct thread *td, struct fkvm_unset_user_mem_region_args *uap)
{
	struct guestvm *guest_vm;

	if (!fkvm_loaded)
		return ENODEV;

	guest_vm = PROC_GET_GUESTVM(td->td_proc);
	if (guest_vm == NULL) {
		printf("PROC_GET_GUESTVM -> NULL\n");
		return ENODEV;
	}

	vm_offset_t start;
	vm_offset_t end;

	vm_map_t guest_vm_map;

	int error;

	start = uap->guest_pa;
	end = uap->guest_pa + uap->size - 1;
	printf("start: 0x%" PRIx64 " bytes\n", start);
	printf("end:   0x%" PRIx64 " bytes\n", end);

	guest_vm_map = &guest_vm->sp->vm_map;

	error = vm_map_remove(guest_vm_map, start, end);
	if (error != KERN_SUCCESS)
		return -1;

	return 0;
}

int
fkvm_create_vm(struct thread *td, struct fkvm_create_vm_args *uap)
{
	struct guestvm *guest_vm;

	printf("SYSCALL : fkvm_create_vm\n");

	if (!fkvm_loaded)
		return ENODEV;

	/* Allocate Guest VM */
	guest_vm = fkvm_guestvm_alloc();

	/* Set up the vm address space */
	guest_vm->sp = fkvm_make_vmspace();
	if (guest_vm->sp == NULL) {
		fkvm_guestvm_free(guest_vm);
		return ENOMEM;
	}
	guest_vm->nested_cr3 = vtophys(vmspace_pmap(guest_vm->sp)->pm_pml4);

	printf("guest:\n");
	printf("  vm space: %p\n", guest_vm->sp);
	printf("  vm map:   %p\n", &guest_vm->sp->vm_map);
	printf("  ncr3:     0x%" PRIx64 "\n", guest_vm->nested_cr3);

	PROC_SET_GUESTVM(td->td_proc, guest_vm);

	printf("fkvm_create_vm done\n");
	return 0;
}

static void
fkvm_destroy_vm(struct guestvm *guest_vm)
{
	/* Destroy the VCPUs */
	while (guest_vm->nr_vcpus > 0) {
		guest_vm->nr_vcpus--;
		fkvm_vcpu_destroy(guest_vm->vcpus[guest_vm->nr_vcpus]);
		guest_vm->vcpus[guest_vm->nr_vcpus] = NULL;
	}

	/* Destroy the vmspace */
	if (guest_vm->sp != NULL)
		fkvm_destroy_vmspace(guest_vm->sp);

	/* Destroy the Guest VM itself */
	fkvm_guestvm_free(guest_vm);
}

static int 
intercept_ioio(struct vcpu *vcpu, struct kvm_run *kvm_run,
               uint64_t ioio_info, uint64_t next_rip)
{
	struct vmcb *vmcb = vcpu->vmcb;

	kvm_run->u.io.string =  (ioio_info & STR_MASK) >> STR_SHIFT;

	kvm_run->u.io.port = ioio_info >> PORT_SHIFT;
	kvm_run->u.io.in = ioio_info & TYPE_MASK;

	kvm_run->u.io.size = (ioio_info & SIZE_MASK) >> SIZE_SHIFT;

	/* We need to remove the Interrupt Shadow Flag from the VMCB (see 15.20.5 in AMD_Vol2) */
	vmcb->control.intr_shadow = 0;

	kvm_run->u.io.rep = (ioio_info & REP_MASK) >> REP_SHIFT;
	/* TODO: Research more into Direction Flag checked in KVM; DF bit in RFLAGS */

	kvm_run->u.io.next_rip = next_rip;

	return 0;
}

static void
intercept_shutdown(struct vcpu *vcpu)
{
	/* TODO */
	struct vmcb *vmcb = vcpu->vmcb;
	print_vmcb(vmcb);
#if 0
	memset(vmcb, 0, PAGE_SIZE);
	fkvm_vmcb_init(vmcb);
#endif
}

int
fkvm_vm_run(struct thread *td, struct fkvm_vm_run_args *uap)
{
	struct vcpu *vcpu;
	struct guestvm *guest_vm;
	struct vmcb *vmcb;
	int error;
	int ret = 0;
	int num_runs = 0;
	struct kvm_run kvm_run;

	if (!fkvm_loaded)
		return ENODEV;

	vcpu = TD_GET_VCPU(td);
	if (vcpu == NULL)
		return ENODEV;

	guest_vm = vcpu->guest_vm;
	vmcb = vcpu->vmcb;

	error = copyin(uap->run, &kvm_run, sizeof(struct kvm_run));
	if (error != 0)
		return error;

	fkvm_set_cr8(vcpu, kvm_run.cr8);

	kvm_run.exit_reason = KVM_EXIT_CONTINUE;

	while(kvm_run.exit_reason == KVM_EXIT_CONTINUE) {
		fkvm_vcpu_run(vcpu);

		switch (vmcb->control.exit_code) {

		case VMCB_EXIT_EXCP_BASE ... (VMCB_EXIT_EXCP_BASE + 31): {
			int excp_vector;

			excp_vector = vmcb->control.exit_code - VMCB_EXIT_EXCP_BASE;

			printf("VMCB_EXIT_EXCP_BASE, exception vector: 0x%x\n",
			       excp_vector);
			print_vmcb(vmcb);
			kvm_run.u.excp.vector = excp_vector;
			kvm_run.exit_reason = KVM_EXIT_EXCP;
			break;
		}

		case VMCB_EXIT_INTR: {
			//printf("VMCB_EXIT_INTR - nothing to do\n");
			/* Handled by host OS already */
			kvm_run.exit_reason = KVM_EXIT_CONTINUE;
			break;
		}

		case VMCB_EXIT_NPF: {
			/* EXITINFO1 contains fault error code */
			/* EXITINFO2 contains the guest physical address causing the fault. */

			u_int64_t fault_code;
			u_int64_t fault_gpa;

			vm_prot_t fault_type;
			int fault_flags;
			int rc;

			fault_code = vmcb->control.exit_info_1;
			fault_gpa  = vmcb->control.exit_info_2;
			kvm_run.exit_reason = KVM_EXIT_CONTINUE;

#if 0
			printf("VMCB_EXIT_NPF:\n");
			printf("gpa=0x%" PRIx64 "\n", fault_gpa);
			printf("fault code=0x%" PRIx64 " [P=%x, R/W=%x, U/S=%x, I/D=%x]\n",
			       fault_code,
			       (fault_code & PGEX_P) != 0,
			       (fault_code & PGEX_W) != 0,
			       (fault_code & PGEX_U) != 0,
			       (fault_code & PGEX_I) != 0);
#endif
			if (fault_code & PGEX_W)
				fault_type = VM_PROT_WRITE;
			else if (fault_code & PGEX_I)
				fault_type = VM_PROT_EXECUTE;
			else
				fault_type = VM_PROT_READ;
			
			fault_flags = 0; /* TODO: is that right? */
			rc = vm_fault(&guest_vm->sp->vm_map, (fault_gpa & (~PAGE_MASK)), fault_type, fault_flags);
			if (rc != KERN_SUCCESS) {
				printf("vm_fault failed: %d\n", rc);
				kvm_run.u.mmio.fault_gpa = fault_gpa;
				kvm_run.u.mmio.rip = vcpu->regs[VCPU_REGS_RIP];
				kvm_run.u.mmio.cs_base = vmcb->save.cs.base;
				kvm_run.exit_reason = KVM_EXIT_MMIO;
			}

			break;
		}
		case VMCB_EXIT_NMI:
			kvm_run.exit_reason = KVM_EXIT_NMI;
			break;
		case VMCB_EXIT_HLT:
			vcpu->regs[VCPU_REGS_RIP]++; /* skip HLT, opcode F4 */
			kvm_run.exit_reason = KVM_EXIT_HLT;
			break;
		case VMCB_EXIT_SHUTDOWN:
			intercept_shutdown(vcpu);
			kvm_run.exit_reason = KVM_EXIT_SHUTDOWN;
			break;
		case VMCB_EXIT_IOIO:
			error = intercept_ioio(vcpu, &kvm_run,
				       vmcb->control.exit_info_1,
				       vmcb->control.exit_info_2);
			if (error)
				kvm_run.exit_reason = KVM_EXIT_UNKNOWN;
			else
				kvm_run.exit_reason = KVM_EXIT_IO;
			break;
		case VMCB_EXIT_MSR: {
			int wrmsr;
			uint32_t msr;
			union {
				struct {
					uint32_t low;
					uint32_t high;
				} split;
				uint64_t full;
			} value;

			wrmsr = vmcb->control.exit_info_1;
			msr = (uint32_t) vcpu->regs[VCPU_REGS_RCX];

			printf("VMCB_EXIT_MSR:\n"
			       "  %s msr 0x%" PRIx64 "\n",
			       wrmsr ? "write to" : "read from",
			       vcpu->regs[VCPU_REGS_RCX]);

			if (!wrmsr) { /* rdmsr */
				error = fkvm_get_reg_msr(vcpu, msr, &value.full);
				if (error != 0) {
					ret = ENOSYS;
					kvm_run.exit_reason = KVM_EXIT_UNKNOWN;
					break;
				}

				vcpu->regs[VCPU_REGS_RDX] = (uint64_t) value.split.high;
				vcpu->regs[VCPU_REGS_RAX] = (uint64_t) value.split.low;
			}
			else { /* wrmsr */
				value.split.high = (uint32_t) vcpu->regs[VCPU_REGS_RDX];
				value.split.low  = (uint32_t) vcpu->regs[VCPU_REGS_RAX];

				error = fkvm_set_reg_msr(vcpu, msr, value.full);
				if (error != 0) {
					ret = ENOSYS;
					kvm_run.exit_reason = KVM_EXIT_UNKNOWN;
					break;
				}

			}

			vcpu->regs[VCPU_REGS_RIP] += 2;
			break;
		}
		case VMCB_EXIT_CPUID: {
			kvm_run.u.cpuid.fn = (uint32_t) vcpu->regs[VCPU_REGS_RAX];
			kvm_run.exit_reason = KVM_EXIT_CPUID;
			break;
		}
		case VMCB_EXIT_WBINVD: {
			/* TODO: stop ignoring this intercept when we have more than 1-cpu guests */
			vcpu->regs[VCPU_REGS_RIP] += 2;
			break;
		}

		case VMCB_EXIT_READ_CR0 ... VMCB_EXIT_READ_CR8: {
			kvm_run.u.cr.is_write = 0;
			kvm_run.u.cr.cr_num = vmcb->control.exit_code & 0xF;
			kvm_run.exit_reason = KVM_EXIT_CR;
			break;
		}
		case VMCB_EXIT_WRITE_CR0 ... VMCB_EXIT_WRITE_CR8: {
			kvm_run.u.cr.is_write = 1;
			kvm_run.u.cr.cr_num = vmcb->control.exit_code & 0xF;
			kvm_run.exit_reason = KVM_EXIT_CR;
			break;
		}
#if 0
		case VMCB_EXIT_READ_DR0 ... VMCB_EXIT_READ_DR7: {
			kvm_run.u.dr.is_write = 0;
			kvm_run.u.dr.dr_num = vmcb->control.exit_code & 0xF;
			kvm_run.exit_reason = KVM_EXIT_DR;
			break;
		}
		case VMCB_EXIT_WRITE_DR0 ... VMCB_EXIT_WRITE_DR7: {
			kvm_run.u.dr.is_write = 1;
			kvm_run.u.dr.dr_num = vmcb->control.exit_code & 0xF;
			kvm_run.exit_reason = KVM_EXIT_DR;
			break;
		}
#endif

		case VMCB_EXIT_SMI:
		case VMCB_EXIT_INIT:
		case VMCB_EXIT_VINTR:
		case VMCB_EXIT_CR0_SEL_WRITE:
		case VMCB_EXIT_INVD:
		case VMCB_EXIT_INVLPG:
		case VMCB_EXIT_INVLPGA:
		case VMCB_EXIT_TASK_SWITCH:
		case VMCB_EXIT_VMRUN:
		case VMCB_EXIT_VMMCALL:
		case VMCB_EXIT_VMLOAD:
		case VMCB_EXIT_VMSAVE:
		case VMCB_EXIT_STGI:
		case VMCB_EXIT_CLGI:
		case VMCB_EXIT_SKINIT:
		case VMCB_EXIT_MONITOR:
		case VMCB_EXIT_MWAIT_UNCOND:
		default:
			printf("Unhandled vmexit:\n"
			       "  code:  0x%" PRIx64 "\n"
			       "  info1: 0x%" PRIx64 "\n"
			       "  info2: 0x%" PRIx64 "\n",
			       vmcb->control.exit_code,
			       vmcb->control.exit_info_1,
			       vmcb->control.exit_info_2);
			print_vmcb(vmcb);
			ret = ENOSYS;
			kvm_run.exit_reason = KVM_EXIT_UNKNOWN;
		}

		num_runs++;
		if (num_runs == 20) //TODO: make this a #define
			break;
	}

//	printf("\n\n");

	/* we're going up to userspace - set the out fields of kvm_run: */

#define IF_MASK 0x00000200
	kvm_run.if_flag = !!(vcpu->vmcb->save.rflags & IF_MASK);

	/* TODO: kvm adds a check to see if in-kernel interrupt queues are empty */
	kvm_run.ready_for_interrupt_injection = kvm_run.if_flag &&
	                         !vcpu->vmcb->control.intr_shadow;

	/* TODO kvm_run.ready_for_nmi_injection = ...; */

	kvm_run.cr8 = fkvm_get_cr8(vcpu);


	/* TODO: check copyout ret val */
	copyout(&kvm_run, uap->run, sizeof(struct kvm_run));
//	printf("sizeof(struct kvm_run) = %" PRIu64 "\n", sizeof(struct kvm_run));

	return ret;
}

int
fkvm_create_vcpu(struct thread *td, struct fkvm_create_vcpu_args *uap)
{
	struct guestvm *guest_vm;
	struct vcpu *vcpu;

	if (!fkvm_loaded)
		return ENODEV;

	guest_vm = PROC_GET_GUESTVM(td->td_proc);
	if (guest_vm == NULL) {
		printf("PROC_GET_GUESTVM -> NULL\n");
		return ENODEV;
	}

	/* Allocate VCPU */
	printf("fkvm_create_vcpu: td = %p\n", td);
	vcpu = fkvm_vcpu_create(guest_vm);
	fkvm_guestvm_add_vcpu(guest_vm, vcpu);

	TD_SET_VCPU(td, vcpu);
	printf("fkvm_create_vcpu: vcpu = %p\n", vcpu);
	return 0;
}

static int
fkvm_check_cpu_extension(void)
{
	u_int cpu_exthigh;
	u_int regs[4];
	u_int64_t vmcr;

	printf("fkvm_check_cpu_extension\n");

	/* Assumption: the architecture supports the cpuid instruction */

	/* Check if CPUID extended function 8000_0001h is supported. */
	do_cpuid(0x80000000, regs);
	cpu_exthigh = regs[0];
	
	printf("cpu_exthigh = %u\n", cpu_exthigh);

	if(cpu_exthigh >= 0x80000001) {
		/* Execute CPUID extended function 8000_0001h */
		do_cpuid(0x80000001, regs);
		printf("EAX = %u\n", regs[0]);
	
		if((regs[0] & 0x2) == 0) { /* Check SVM bit */
			printf("SVM not available\n");
			goto fail;	/* SVM not available */
		}

		vmcr = rdmsr(0xc0010114); /* Read VM_CR MSR */
		if((vmcr & 0x8) == 0) { /* Check SVMDIS bit */	
			printf("vmcr = %" PRIx64 "\n", vmcr);
			printf("SVM allowed\n");
			return KERN_SUCCESS;	/* SVM allowed */
		}	
	
		/* Execute CPUID extended function 8000_000ah */
		do_cpuid(0x8000000a, regs);
		if((regs[3] & 0x2) == 0) { /* Check SVM_LOCK bit */
			/* SVM disabled at bios; not unlockable. 
			 * User must change a BIOS setting to enable SVM.
			 */
			printf("EDX = %u\n", regs[3]);
			printf("SVM disabled at bios\n");
			goto fail;   
		} else {
			/* TODO:
			 * SVM may be unlockable;
			 * consult the BIOS or TPM to obtain the key. 
			 */
			printf("EDX = %u\n", regs[3]);
			printf("SVM maybe unlockable\n");
			goto fail;
		}
	}
fail:
	return KERN_FAILURE;
}

static void
fkvm_proc_exit(void *arg, struct proc *p)
{
	struct guestvm *guest_vm;

	guest_vm = PROC_GET_GUESTVM(p);
	if (guest_vm == NULL)
		return;

	fkvm_destroy_vm(guest_vm);
	PROC_SET_GUESTVM(p, NULL);
}

static void
fkvm_load(void *unused)
{
	u_int64_t	efer;
	int error;

	printf("fkvm_load\n");
	printf("sizeof(struct vmcb) = %" PRIx64 "\n", sizeof(struct vmcb));

	hsave_area = NULL;
	iopm = NULL;
	msrpm = NULL;

	/* check if SVM is supported */
	error = fkvm_check_cpu_extension();
	if(error != KERN_SUCCESS) {
		printf("ERROR: SVM extension not available\n");
		return;
	}

	exit_tag = EVENTHANDLER_REGISTER(process_exit, fkvm_proc_exit, NULL,
		EVENTHANDLER_PRI_ANY);

	/* allocate structures */
	hsave_area = fkvm_hsave_area_alloc();
	iopm = fkvm_iopm_alloc();
	msrpm = fkvm_msrpm_alloc();

	/* Initialize structures */
	fkvm_hsave_area_init(hsave_area);
	fkvm_iopm_init(iopm);
	fkvm_msrpm_init(msrpm);

	/* Enable SVM in EFER */
	efer = rdmsr(MSR_EFER);
	printf("EFER = %" PRIx64 "\n", efer);
	wrmsr(MSR_EFER, efer | EFER_SVME);
	efer = rdmsr(MSR_EFER);
	printf("new EFER = %" PRIx64 "\n", efer);

	/* Write Host save address in MSR_VM_HSAVE_PA */
	wrmsr(MSR_VM_HSAVE_PA, vtophys(hsave_area));

	fkvm_loaded = 1;
}
SYSINIT(fkvm, SI_SUB_PSEUDO, SI_ORDER_MIDDLE, fkvm_load, NULL);

static void
fkvm_unload(void *unused)
{
	printf("fkvm_unload\n");

	if (!fkvm_loaded) {
		printf("fkvm_unload: fkvm not loaded");
		return;
	}

	EVENTHANDLER_DEREGISTER(process_exit, exit_tag);

	if (msrpm != NULL) {
		fkvm_msrpm_free(iopm);
		msrpm = NULL;
	}
	if (iopm != NULL) {
		fkvm_iopm_free(iopm);
		iopm = NULL;
	}
	if (hsave_area != NULL) {
		fkvm_hsave_area_free(hsave_area);
		hsave_area = NULL;
	}
}
SYSUNINIT(fkvm, SI_SUB_PSEUDO, SI_ORDER_MIDDLE, fkvm_unload, NULL);

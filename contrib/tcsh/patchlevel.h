/* $Header: /p/tcsh/cvsroot/tcsh/patchlevel.h,v 3.161 2007/03/03 20:01:26 christos Exp $ */
/*
 * patchlevel.h: Our life story.
 */
#ifndef _h_patchlevel
#define _h_patchlevel

#define ORIGIN "Astron"
#define REV 6
#define VERS 15
#define PATCHLEVEL 0
#define DATE "2007-03-03"

#endif /* _h_patchlevel */
